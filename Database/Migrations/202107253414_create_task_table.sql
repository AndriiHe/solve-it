DO
$$
DECLARE migration_name VARCHAR(500) = '202107253414_create_task_table';
BEGIN
IF NOT EXISTS(SELECT * FROM migration WHERE name = migration_name) THEN

    INSERT INTO migration(name) VALUES (migration_name);

    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

    CREATE TABLE IF NOT EXISTS task
    (
        id         UUID          NOT NULL,
        expression VARCHAR(500)  NOT NULL,
        latex      VARCHAR(1000) NOT NULL,
        bounds     jsonb         NOT NULL,
        log_type   INT           NOT NULL DEFAULT 0,
        created_at TIMESTAMP     NOT NULL DEFAULT NOW(),
        updated_at TIMESTAMP     NOT NULL DEFAULT NOW(),
        created_by UUID          NOT NULL,
        PRIMARY KEY (id),
        CONSTRAINT fk_user_user_id FOREIGN KEY(created_by) REFERENCES "user"(id)
    );

end if;
end;
$$
