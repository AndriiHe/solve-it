DO
$$
DECLARE migration_name VARCHAR(500) = '202107252413_create_user_table';
BEGIN
IF NOT EXISTS(SELECT * FROM migration WHERE name = migration_name) THEN

    INSERT INTO migration(name) VALUES (migration_name);

    CREATE TABLE IF NOT EXISTS "user"
    (
        id         UUID          NOT NULL,
        login      VARCHAR(1000) NOT NULL,
        password   VARCHAR(1000) NOT NULL,
        created_at TIMESTAMP     NOT NULL DEFAULT NOW(),
        updated_at TIMESTAMP     NOT NULL DEFAULT NOW(),
        PRIMARY KEY (id),
        UNIQUE (login)
    );

end if;
end;
$$
