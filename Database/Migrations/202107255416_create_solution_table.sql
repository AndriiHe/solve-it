DO
$$
DECLARE migration_name VARCHAR(500) = '202107255416_create_solution_table';
BEGIN
IF NOT EXISTS(SELECT * FROM migration WHERE name = migration_name) THEN
    INSERT INTO migration(name) VALUES (migration_name);

    CREATE TABLE IF NOT EXISTS solution
    (
        id          UUID         NOT NULL,
        job_id      UUID         NOT NULL,
        value       FLOAT(53)    NULL,
        accuracy    FLOAT(53)    NULL,
        status      INT          NOT NULL,
        iterations  INT          NULL,
        stop_reason INT          NULL,
        position    jsonb        NULL,
        details     varchar(500) NULL,
        created_at  TIMESTAMP    NOT NULL DEFAULT NOW(),
        updated_at  TIMESTAMP    NOT NULL DEFAULT NOW(),
        PRIMARY KEY (id),
        CONSTRAINT fk_job_job_id FOREIGN KEY(job_id) REFERENCES job(id)
    );

end if;
end;
$$
