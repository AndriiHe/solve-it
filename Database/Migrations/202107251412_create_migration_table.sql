CREATE TABLE IF NOT EXISTS migration
(
    name       VARCHAR(500) NOT NULL,
    created_at TIMESTAMP    NOT NULL DEFAULT NOW(),
    PRIMARY KEY (name)
);
