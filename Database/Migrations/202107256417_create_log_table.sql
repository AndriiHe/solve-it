DO
$$
DECLARE migration_name VARCHAR(500) = '202107256417_create_log_table';
BEGIN
IF NOT EXISTS(SELECT * FROM migration WHERE name = migration_name) THEN
    INSERT INTO migration(name) VALUES (migration_name);

    CREATE TABLE IF NOT EXISTS log
    (
        id          UUID         NOT NULL,
        solution_id UUID         NOT NULL,
        position    jsonb        NULL,
        value       jsonb        NULL,
        details     jsonb        NULL,
        created_at  TIMESTAMP    NOT NULL DEFAULT NOW(),
        updated_at  TIMESTAMP    NOT NULL DEFAULT NOW(),
        PRIMARY KEY (id),
        CONSTRAINT fk_solution_solution_id FOREIGN KEY(solution_id) REFERENCES solution(id)
    );

end if;
end;
$$
