DO
$$
DECLARE migration_name VARCHAR(500) = '202107254415_create_job_table';
BEGIN
IF NOT EXISTS(SELECT * FROM migration WHERE name = migration_name) THEN
    INSERT INTO migration(name) VALUES (migration_name);

    CREATE TABLE IF NOT EXISTS job
    (
        id         UUID         NOT NULL,
        task_id    UUID         NOT NULL,
        options    jsonb        NOT NULL,
        created_at TIMESTAMP    NOT NULL DEFAULT NOW(),
        updated_at TIMESTAMP    NOT NULL DEFAULT NOW(),
        PRIMARY KEY (id),
        CONSTRAINT fk_task_task_id FOREIGN KEY(task_id) REFERENCES task(id)
    );

end if;
end;
$$
