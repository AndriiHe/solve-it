﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SolveIt.FuzzySearch.DefuzzifyAlgorithm;
using SolveIt.FuzzySearch.Engines;
using SolveIt.FuzzySearch.Extensions;
using SolveIt.FuzzySearch.MembershipFunctions;
using SolveIt.FuzzySearch.Rules;
using SolveIt.FuzzySearch.Variables;

namespace SolveIt.FuzzySearch.Tests
{
    public class FuzzyEngineTest
    {
        [Test]
        public void Test()
        {
            const string SMALL = "SMALL", MEDIUM = "MEDIUM", LARGE = "LARGE";

            var df = new InputVariable("df", new List<MembershipFunction>
            {
                new TriangleMembershipFunction(SMALL, 0, 0, 33.333),
                new TrapezoidMembershipFunction(MEDIUM, 0, 33.333, 66.666, 100),
                new TriangleMembershipFunction(LARGE, 66.66, 100, 100)
            });

            var c1 = new OutputVariable("c1", new List<MembershipFunction>
            {
                new TriangleMembershipFunction(SMALL, 0, 0, 2),
                new TriangleMembershipFunction(MEDIUM, 0, 2, 4),
                new TriangleMembershipFunction(LARGE, 2, 4, 4)
            });

            var c2 = new OutputVariable("c2", new List<MembershipFunction>
            {
                new TriangleMembershipFunction(SMALL, 0, 0, 2),
                new TriangleMembershipFunction(MEDIUM, 0, 2, 4),
                new TriangleMembershipFunction(LARGE, 2, 4, 4)
            });

            var rules = new List<Rule>
            {
                Rule.If(df.Is(SMALL)).Then(c1.Is(LARGE).And(c2.Is(SMALL))),
                Rule.If(df.Is(MEDIUM)).Then(c1.Is(MEDIUM).And(c2.Is(MEDIUM))),
                Rule.If(df.Is(LARGE)).Then(c1.Is(SMALL).And(c2.Is(LARGE))),
            };

            var values = new Dictionary<string, double>
            {
                { "df", 25 }
            };

            var fuzzyEngine = new FuzzyEngine(new CenterOfGravityAlgorithm());
            var result = fuzzyEngine.Defuzzify(values, rules, 181);

            Assert.That(result.First(kv => kv.Key == "c1").Value, Is.EqualTo(2.061).Within(0.001));
            Assert.That(result.First(kv => kv.Key == "c2").Value, Is.EqualTo(1.938).Within(0.001));
        }

        [Test]
        public void Test1()
        {
            const string SMALL = "SMALL", MEDIUM = "MEDIUM", LARGE = "LARGE";

            var x1 = new InputVariable("x1", new List<MembershipFunction>
            {
                new TriangleMembershipFunction(SMALL, 0, 25, 50),
                new TriangleMembershipFunction(MEDIUM, 25, 50, 75),
                new TriangleMembershipFunction(LARGE, 50, 75, 100)
            });

            var x2 = new InputVariable("x2", new List<MembershipFunction>
            {
                new TriangleMembershipFunction(SMALL, 0, 25, 50),
                new TriangleMembershipFunction(MEDIUM, 25, 50, 75),
                new TriangleMembershipFunction(LARGE, 50, 75, 100)
            });

            var y1 = new OutputVariable("y1", new List<MembershipFunction>
            {
                new TriangleMembershipFunction(SMALL, 0, 25, 50),
                new TriangleMembershipFunction(MEDIUM, 25, 50, 75),
                new TriangleMembershipFunction(LARGE, 50, 75, 100)
            });

            var y2 = new OutputVariable("y2", new List<MembershipFunction>
            {
                new TriangleMembershipFunction(SMALL, 0, 25, 50),
                new TriangleMembershipFunction(MEDIUM, 25, 50, 75),
                new TriangleMembershipFunction(LARGE, 50, 75, 100)
            });

            var rules = new List<Rule>
            {
                Rule.If(x1.Is(SMALL).And(x2.Is(SMALL))).Then(y1.Is(SMALL).And(y2.Is(LARGE))),
                Rule.If(x1.Is(MEDIUM).And(x2.Is(MEDIUM))).Then(y1.Is(MEDIUM).And(y2.Is(MEDIUM))),
                Rule.If(x1.Is(LARGE).And(x2.Is(LARGE))).Then(y1.Is(LARGE).And(y2.Is(SMALL))),
                Rule.If(x1.Is(SMALL).And(x2.Is(MEDIUM))).Then(y1.Is(SMALL).And(y2.Is(LARGE))),
                Rule.If(x1.Is(MEDIUM).And(x2.Is(SMALL))).Then(y1.Is(SMALL).And(y2.Is(LARGE))),
                Rule.If(x1.Is(LARGE).And(x2.Is(MEDIUM))).Then(y1.Is(LARGE).And(y2.Is(SMALL))),
                Rule.If(x1.Is(MEDIUM).And(x2.Is(LARGE))).Then(y1.Is(LARGE).And(y2.Is(SMALL))),
                Rule.If(x1.Is(LARGE).And(x2.Is(SMALL))).Then(y1.Is(MEDIUM).And(y2.Is(MEDIUM))),
                Rule.If(x1.Is(SMALL).And(x2.Is(LARGE))).Then(y1.Is(MEDIUM).And(y2.Is(MEDIUM))),
            };

            var values = new Dictionary<string, double>
            {
                { "x1", 35 },
                { "x2", 75 },
            };

            var fuzzyEngine = new FuzzyEngine(new CenterOfGravityAlgorithm());
            var result = fuzzyEngine.Defuzzify(values, rules);

            Assert.That(result.First(kv => kv.Key == "y1").Value, Is.EqualTo(60.483).Within(0.001));
            Assert.That(result.First(kv => kv.Key == "y2").Value, Is.EqualTo(39.516).Within(0.001));
        }
    }
}
