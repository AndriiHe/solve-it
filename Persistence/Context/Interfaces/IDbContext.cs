using System.Data;

namespace SolveIt.Persistence.Context.Interfaces
{
    public interface IDbContext
    {
        public IDbConnection GetConnection();
    }
}
