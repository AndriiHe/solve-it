using System.Data;
using Microsoft.Extensions.Options;
using Npgsql;
using SolveIt.Common.Model.Options;
using SolveIt.Persistence.Context.Interfaces;

namespace SolveIt.Persistence.Context
{
    public class DbContext : IDbContext
    {
        private readonly string _connectionString;

        public DbContext(IOptions<DbOptions> options)
        {
            _connectionString = options.Value.ConnectionString;
        }

        public IDbConnection GetConnection()
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            return connection;
        }
    }
}
