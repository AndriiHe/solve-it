using SolveIt.Persistence.Context.Interfaces;

namespace SolveIt.Persistence.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly IDbContext Context;

        protected BaseRepository(IDbContext context)
        {
            Context = context;
        }
    }
}
