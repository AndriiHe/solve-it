using System;
using System.Threading.Tasks;
using SolveIt.Common.Model;

namespace SolveIt.Persistence.Repositories.Interfaces
{
    public interface ISolutionRepository
    {
        public Task<Solution> GetById(Guid id);
    }
}
