using System;
using System.Threading.Tasks;
using SolveIt.Common.Model;

namespace SolveIt.Persistence.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public Task<User> GetUserByEmail(string email);
        public Task<Guid> CreateUser(User user);
        public Task<bool> IsUserExists(string email);
    }
}
