using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SolveIt.Common.Model;

namespace SolveIt.Persistence.Repositories.Interfaces
{
    public interface ITaskRepository
    {
        public Task<Guid> Create(SwarmIntelligenceTask task, Guid userId);

        public Task<SwarmIntelligenceTask> GetById(Guid id, Guid userId);

        public Task<IEnumerable<SwarmIntelligenceJob>> GetTaskJobs(Guid taskId);

        public Task UpdateSolution(Solution solution, Guid id);

        public Task UpdateSolutionStatus(Guid id, JobStatus status, string message);

        public Task<IEnumerable<SwarmIntelligenceTask>> GetTasks(int page, int perPage, Guid userId);
        public Task<int> GetTasksPagination(Guid userId);
    }
}
