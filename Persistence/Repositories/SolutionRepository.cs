using System;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using SolveIt.Common.Model;
using SolveIt.Persistence.Context.Interfaces;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Persistence.Repositories
{
    public class SolutionRepository: BaseRepository, ISolutionRepository
    {
        public SolutionRepository(IDbContext context) : base(context)
        {
        }

        public async Task<Solution> GetById(Guid id)
        {
            using var connection = Context.GetConnection();
            var solutions = await connection.QueryAsync<string>(
                @"
                    SELECT row_to_json(row)
                    FROM (
                        SELECT s.id, s.value, s.position, l.position AS bestPositionLog, l.value AS bestValueLog, l.details AS PositionLog, s.iterations AS IterationsCount
                        FROM solution s
                        LEFT JOIN log l on s.id = l.solution_id
                        WHERE s.id = @Id
                    ) AS row
                ",
                new { Id = id }
            );
            return solutions.Select(JsonConvert.DeserializeObject<Solution>).FirstOrDefault();
        }
    }
}
