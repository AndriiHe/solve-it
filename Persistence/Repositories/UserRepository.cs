using System;
using System.Threading.Tasks;
using Dapper;
using SolveIt.Common.Model;
using SolveIt.Persistence.Context.Interfaces;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Persistence.Repositories
{
    public class UserRepository: BaseRepository, IUserRepository
    {
        public UserRepository(IDbContext context) : base(context)
        {
        }

        public async Task<User> GetUserByEmail(string email)
        {
            using var connection = Context.GetConnection();
            var user = await connection.QuerySingleOrDefaultAsync<User>(
                "SELECT id, login AS username, password FROM \"user\" WHERE login = @Email;",
                new { Email = email }
            );
            return user;
        }

        public async Task<Guid> CreateUser(User user)
        {
            using var connection = Context.GetConnection();
            var id = await connection.ExecuteScalarAsync<Guid>(
                "INSERT INTO \"user\"(id, login, password) VALUES (uuid_generate_v4(), @Username, @Password) RETURNING id;",
                user
            );
            return id;
        }

        public async Task<bool> IsUserExists(string email)
        {
            using var connection = Context.GetConnection();
            return await connection.ExecuteScalarAsync<bool>(
                "SELECT COUNT(1) FROM \"user\" WHERE login=@Email;",
            new { Email = email }
            );
        }
    }
}
