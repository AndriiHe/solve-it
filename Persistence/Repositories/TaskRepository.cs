using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SolveIt.Common.Model;
using SolveIt.Persistence.Context.Interfaces;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Persistence.Repositories
{
    public class TaskRepository: BaseRepository, ITaskRepository
    {
        public TaskRepository(IDbContext context) : base(context) { }

        public async Task<Guid> Create(SwarmIntelligenceTask task, Guid userId)
        {
            using var connection = Context.GetConnection();
            var taskId = await connection.ExecuteScalarAsync<Guid>(
                @"
                    WITH
                        tasks AS (
                            INSERT INTO task (id, expression, latex, bounds, log_type, created_by)
                            SELECT uuid_generate_v4(), row.value->>'expression', row.value->>'latex', row.value->'bounds', (row.value->>'logProcess')::INT, @UserId
                            FROM (SELECT @Task::json AS value ) row
                            RETURNING id
                        ),
                        jobs AS (
                            INSERT INTO job (id, task_id, options)
                            SELECT uuid_generate_v4(), t.id, row.value->'options'
                            FROM (SELECT json_array_elements(@Task::json->'algorithms') AS value) row
                            CROSS JOIN tasks t
                            RETURNING id, (options->'repeatNumber')::INT AS repeat
                        ),
                        solutions AS (
                            INSERT INTO solution (id, job_id, status)
                            SELECT uuid_generate_v4(), j.id, 1
                            FROM jobs j
                            CROSS JOIN generate_series(1, j.repeat)
                            RETURNING id
                        )
                    SELECT id FROM tasks;
                ",
                new { Task = JsonConvert.SerializeObject(task), UserId = userId }
            );
            return taskId;
        }

        public async Task<SwarmIntelligenceTask> GetById(Guid taskId, Guid userId)
        {
            using var connection = Context.GetConnection();
            var tasks = await connection.QueryAsync<string>(
                @"
                    SELECT row_to_json(row)
                    FROM (
                        SELECT id, expression, latex, bounds, log_type as logProcess,
                            (
                                SELECT array_to_json(array_agg(row_to_json(tj)))
                                FROM (
                                    SELECT id, options,
                                        (
                                            SELECT array_to_json(array_agg(row_to_json(js)))
                                            FROM (
                                                SELECT id, value, position, iterations AS ""iterationsCount"", accuracy
                                                FROM solution
                                                WHERE j.id = job_id
                                            ) js
                                        ) AS solutions
                                    FROM job j
                                    WHERE t.id = task_id
                                ) tj
                            ) AS algorithms
                        FROM task t
                        WHERE t.id = @TaskId AND t.created_by = @UserId
                    ) AS row
                ",
                new { TaskId = taskId, UserId = userId }
            );
            return tasks.Select(JsonConvert.DeserializeObject<SwarmIntelligenceTask>).FirstOrDefault();

        }

        public async Task<IEnumerable<SwarmIntelligenceJob>> GetTaskJobs(Guid taskId)
        {
            using var connection = Context.GetConnection();
            var jobs = await connection.QueryAsync<string>(
                @"
                    SELECT row_to_json(row)
                    FROM (
                        SELECT s.id, j.options, t.bounds, t.expression, t.log_type as logType
                        FROM solution s
                        LEFT JOIN job j ON j.id = s.job_id
                        LEFT JOIN task t ON t.id = j.task_id
                        WHERE t.id = @TaskId
                    ) AS row
                ",
            new { TaskId = taskId }
            );
            return jobs.Select(JsonConvert.DeserializeObject<SwarmIntelligenceJob>);
        }

        public async Task UpdateSolution(Solution solution, Guid id)
        {
            var settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            using var connection = Context.GetConnection();
            await connection.ExecuteAsync(
                @"
                    WITH logs AS (
                        INSERT INTO log (id, solution_id, position, value, details, updated_at)
                        VALUES (uuid_generate_v4(), @Id, @Solution::json->'bestPositionLog', @Solution::json->'bestValueLog', @Solution::json->'positionLog', NOW())
                        RETURNING id
                    )
                    UPDATE solution
                    SET
                        value = (@Solution::json->>'value')::decimal,
                        position = @Solution::json->'position',
                        iterations = (@Solution::json->>'iterationsCount')::int,
                        accuracy = (@Solution::json->>'accuracy')::decimal,
                        status = 2,
                        updated_at = NOW()
                    WHERE id = @Id;
                ",
                new { Id = id, Solution = JsonConvert.SerializeObject(solution, settings) }
            );
        }

        public async Task UpdateSolutionStatus(Guid id, JobStatus status, string message)
        {
            using var connection = Context.GetConnection();
            await connection.ExecuteAsync(
                @"UPDATE solution SET details = @Details, status = @Status, updated_at = NOW() WHERE id = @Id;",
                new { Id = id, Status = status, Details = message }
            );
        }

        public async Task<IEnumerable<SwarmIntelligenceTask>> GetTasks(int page, int perPage, Guid userId)
        {
            using var connection = Context.GetConnection();
            var tasks = await connection.QueryAsync<string>(
                @"
                    SELECT row_to_json(row)
                    FROM (
                        SELECT id, expression, latex, bounds,
                            (
                                SELECT array_to_json(array_agg(row_to_json(tj)))
                                FROM (
                                    SELECT id, options
                                    FROM job j
                                    WHERE t.id = task_id
                                ) tj
                            ) AS algorithms
                        FROM task t
                        WHERE t.created_by = @UserId
                        LIMIT @Limit
                        OFFSET @Offset
                    ) AS row
                ",
                new { UserId = userId, Limit = perPage, Offset = (page - 1) * perPage }
            );
            return tasks.Select(JsonConvert.DeserializeObject<SwarmIntelligenceTask>);
        }

        public async Task<int> GetTasksPagination(Guid userId)
        {
            using var connection = Context.GetConnection();
            var count = await connection.ExecuteScalarAsync<int>(
                "SELECT COUNT(id) FROM task WHERE created_by=@UserId",
                new { UserId = userId }
            );
            return count;
        }
    }
}
