using System;
using System.Collections.Generic;
using SolveIt.FuzzySearch.Rules;

namespace SolveIt.FuzzySearch.DefuzzifyAlgorithm
{
    public interface IDefuzzifyAlgorithm
    {
        public IDictionary<string, double> Defuzzify(IEnumerable<Tuple<Conclusion, double>> conclusions, int resolution);
    }
}