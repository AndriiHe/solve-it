using System;
using System.Collections.Generic;
using System.Linq;
using SolveIt.FuzzySearch.Rules;

namespace SolveIt.FuzzySearch.DefuzzifyAlgorithm
{
    public class CenterOfGravityAlgorithm : IDefuzzifyAlgorithm
    {
        public IDictionary<string, double> Defuzzify(IEnumerable<Tuple<Conclusion, double>> conclusions, int resolution)
        {
            var enumerable = conclusions.ToList();
            return enumerable.ToList()
                .GroupBy(t => t.Item1.Variable.Name)
                .Select(g => g.First().Item1.Variable.Name)
                .ToDictionary(variable => variable, variable => Execute(enumerable, variable, resolution));
        }

        private static double Execute(IEnumerable<Tuple<Conclusion, double>> conclusions, string variable, int resolution)
        {
            var cs = conclusions.Where(conclusion => conclusion.Item1.Variable.Name == variable).ToList();

            var min = cs.FirstOrDefault()?.Item1.Variable.Terms.Select(f => f.Min()).Min() ?? 0;
            var max = cs.FirstOrDefault()?.Item1.Variable.Terms.Select(f => f.Max()).Max() ?? 0;
            var values = 0.0;
            var sum = 0.0;
            var range = (max - min) / resolution;

            for (var x = min; x < max; x += range)
            {
                var y = cs.Select(t => Math.Min(t.Item2, t.Item1.Term.Fuzzify(x))).Max();
                values += y;
                sum += x * y;
            }

            return sum / values;
        }
    }
}
