using System.Collections.Generic;
using SolveIt.FuzzySearch.MembershipFunctions;

namespace SolveIt.FuzzySearch.Variables
{
    public class OutputVariable : Variable
    {
        public OutputVariable(string name) : base(name)
        {
        }

        public OutputVariable(string name, ICollection<MembershipFunction> terms) : base(name, terms)
        {
        }
    }
}