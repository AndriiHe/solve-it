using System.Collections.Generic;
using SolveIt.FuzzySearch.MembershipFunctions;

namespace SolveIt.FuzzySearch.Variables
{
    public class InputVariable : Variable
    {
        public InputVariable(string name) : base(name)
        {
        }

        public InputVariable(string name, ICollection<MembershipFunction> terms) : base(name, terms)
        {
        }
    }
}