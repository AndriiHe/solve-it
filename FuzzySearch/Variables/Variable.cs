using System.Collections.Generic;
using System.Linq;
using SolveIt.FuzzySearch.MembershipFunctions;

namespace SolveIt.FuzzySearch.Variables
{
    public abstract class Variable
    {
        public string Name { get; }
        public ICollection<MembershipFunction> Terms { get; set; }

        public Variable(string name)
        {
            Name = name;
            Terms = new List<MembershipFunction>();
        }

        public Variable(string name, ICollection<MembershipFunction> terms)
        {
            Name = name;
            Terms = terms;
        }

        public double Min() => Terms.Select(f => f.Min()).Min();

        public double Max() => Terms.Select(f => f.Max()).Max();

    }
}
