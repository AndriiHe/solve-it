namespace SolveIt.FuzzySearch.MembershipFunctions
{
    public abstract class MembershipFunction
    {
        public string Name { get; }

        protected MembershipFunction(string name)
        {
            Name = name;
        }

        public abstract double Fuzzify(double value);
        public abstract double Min();
        public abstract double Max();
    }
}