namespace SolveIt.FuzzySearch.MembershipFunctions
{
    public class TriangleMembershipFunction : TrapezoidMembershipFunction
    {
        public TriangleMembershipFunction(string name, double a, double b, double c) : base(name, a, b, b, c)
        {
        }
    }
}