namespace SolveIt.FuzzySearch.MembershipFunctions
{
    public class TrapezoidMembershipFunction : MembershipFunction
    {
        private double A { get; }
        private double B { get; }
        private double C { get; }
        private double D { get; }

        public TrapezoidMembershipFunction(string name, double a, double b, double c, double d) : base(name)
        {
            A = a;
            B = b;
            C = c;
            D = d;
        }

        public override double Fuzzify(double value)
        {
            if (A <= value && value < B)
                return (value - A) / (B - A);
            if (B <= value && value <= C)
                return 1;
            if (C < value && value <= D)
                return (D - value) / (D - C);
            return 0;
        }

        public override double Min() => A;

        public override double Max() => D;
    }
}