using System.Collections.Generic;
using SolveIt.FuzzySearch.Rules;

namespace SolveIt.FuzzySearch.Engines
{
    public interface IFuzzyEngine
    {
        public IDictionary<string, double> Defuzzify(IDictionary<string, double> values, IEnumerable<Rule> rules, int resolution = 100);
    }
}
