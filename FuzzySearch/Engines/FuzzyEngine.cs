using System;
using System.Collections.Generic;
using System.Linq;
using SolveIt.FuzzySearch.DefuzzifyAlgorithm;
using SolveIt.FuzzySearch.Rules;

namespace SolveIt.FuzzySearch.Engines
{
    public class FuzzyEngine : IFuzzyEngine
    {
        private IDefuzzifyAlgorithm _algorithm;

        public FuzzyEngine(IDefuzzifyAlgorithm algorithm)
        {;
            _algorithm = algorithm;
        }

        public IDictionary<string, double> Defuzzify(IDictionary<string, double> values, IEnumerable<Rule> rules, int resolution = 100)
        {
            var conclusions = rules
                .ToList()
                .SelectMany((rule, i) => rule.Conclusions.Select(conclusion => new Tuple<Conclusion, double>(conclusion, rule.AggregateConditions(values))));

            return _algorithm.Defuzzify(conclusions, resolution);
        }
    }
}
