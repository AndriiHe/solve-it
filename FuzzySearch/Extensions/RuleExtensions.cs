using System.Collections.Generic;
using System.Linq;
using SolveIt.FuzzySearch.MembershipFunctions;
using SolveIt.FuzzySearch.Rules;
using SolveIt.FuzzySearch.Variables;

namespace SolveIt.FuzzySearch.Extensions
{
    public static class RuleExtensions
    {
        public static Condition Is(this InputVariable variable, MembershipFunction term)
        {
            return new Condition { Variable = variable, Term = term, Operator = OperatorType.Is };
        }

        public static Condition Is(this InputVariable variable, string term)
        {
            var value = variable.Terms.FirstOrDefault(t => t.Name == term);
            return variable.Is(value);
        }

        public static Condition IsNot(this InputVariable variable, MembershipFunction term)
        {
            return new Condition { Variable = variable, Term = term, Operator = OperatorType.IsNot };
        }

        public static Condition IsNot(this InputVariable variable, string term)
        {
            var value = variable.Terms.FirstOrDefault(t => t.Name == term);
            return variable.IsNot(value);
        }

        public static Conclusion Is(this OutputVariable variable, MembershipFunction term)
        {
            return new Conclusion { Variable = variable, Term = term, Operator = OperatorType.Is };
        }

        public static Conclusion Is(this OutputVariable variable, string term)
        {
            var value = variable.Terms.FirstOrDefault(t => t.Name == term);
            return variable.Is(value);
        }

        public static Rule Then(this Rule rule, IEnumerable<Conclusion> conclusions)
        {
            return new Rule { Conclusions = conclusions.ToList(), Conditions = rule.Conditions };
        }
        public static Rule Then(this Rule rule, Conclusion conclusion)
        {
            return new Rule { Conclusions = new List<Conclusion>{ conclusion }, Conditions = rule.Conditions };
        }

        public static IEnumerable<Condition> And(this Condition value, Condition condition)
        {
            return new List<Condition>
            {
                value,
                new Condition { Variable = condition.Variable, Term = condition.Term, Operator = condition.Operator, Conjunction = ConjunctionType.And }
            };
        }

        public static IEnumerable<Condition> And(this IEnumerable<Condition> value, Condition condition)
        {
            return value.Concat(new List<Condition> { new Condition { Variable = condition.Variable, Term = condition.Term, Operator = condition.Operator, Conjunction = ConjunctionType.And } });
        }

        public static IEnumerable<Conclusion> And(this Conclusion value, Conclusion condition)
        {
            return new List<Conclusion>
            {
                value,
                new Conclusion { Variable = condition.Variable, Term = condition.Term, Operator = condition.Operator }
            };
        }

        public static IEnumerable<Conclusion> And(this IEnumerable<Conclusion> value, Conclusion condition)
        {
            return value.Concat(new List<Conclusion> { new Conclusion { Variable = condition.Variable, Term = condition.Term, Operator = condition.Operator } });
        }

        public static IEnumerable<Condition> Or(this Condition value, Condition condition)
        {
            return new List<Condition>
            {
                value,
                new Condition { Variable = condition.Variable, Term = condition.Term, Operator = condition.Operator, Conjunction = ConjunctionType.Or }
            };
        }

        public static IEnumerable<Condition> Or(this IEnumerable<Condition> value, Condition condition)
        {
            return value.Concat(new List<Condition> { new Condition { Variable = condition.Variable, Term = condition.Term, Operator = condition.Operator, Conjunction = ConjunctionType.Or } });
        }
    }
}
