namespace SolveIt.FuzzySearch.Rules
{
    public enum OperatorType
    {
        Is = 0,
        IsNot = 1
    }
}