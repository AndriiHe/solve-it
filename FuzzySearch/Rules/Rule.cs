using System;
using System.Collections.Generic;
using System.Linq;

namespace SolveIt.FuzzySearch.Rules
{
    public class Rule
    {
        public IEnumerable<Condition> Conditions { get; set; }
        public IEnumerable<Conclusion> Conclusions { get; set; }

        public static Rule If(IEnumerable<Condition> conditions)
        {
            return new Rule { Conditions = conditions };
        }
        
        public static Rule If(Condition condition)
        {
            return new Rule { Conditions = new List<Condition>{ condition } };
        }
        
        public double AggregateConditions(IDictionary<string, double> values)
        {
            return Conditions.ToList().Aggregate(1.0, (degree, condition) => Union(degree, condition, values));
        }

        private static double Union(double degree, Statement condition, IDictionary<string, double> values)
        {
            var fuzzifiedValue = condition.Term.Fuzzify(values[condition.Variable.Name]);
            var aggregatedValue = condition.Operator == OperatorType.Is ? fuzzifiedValue : 1 - fuzzifiedValue;
            return condition.Conjunction == ConjunctionType.And ? Math.Min(aggregatedValue, degree) : Math.Max(aggregatedValue, degree);
        }
    }
}