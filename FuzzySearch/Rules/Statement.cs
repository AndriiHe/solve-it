using SolveIt.FuzzySearch.MembershipFunctions;
using SolveIt.FuzzySearch.Variables;

namespace SolveIt.FuzzySearch.Rules
{
    public class Statement
    {
        public MembershipFunction Term;
        public Variable Variable;
        public OperatorType Operator = OperatorType.Is;
        public ConjunctionType Conjunction = ConjunctionType.And;
    }
}