namespace SolveIt.FuzzySearch.Rules
{
    public enum ConjunctionType
    {
        And = 1,
        Or = 0
    }
}