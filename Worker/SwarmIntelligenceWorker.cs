using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SolveIt.Common.MessageQueue.Interfaces;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms;
using SolveIt.Persistence.Repositories.Interfaces;
using SolveIt.SwarmIntelligence;
using SolveIt.SwarmIntelligence.Algorithms;
using SolveIt.SwarmIntelligence.Functions;

namespace SolveIt.Worker
{
    public class SwarmIntelligenceWorker : IHostedService
    {
        private readonly IMessageBroker<SwarmIntelligenceJob> _broker;
        private readonly ITaskRepository _taskRepository;
        private readonly ILogger _logger;

        public SwarmIntelligenceWorker(IMessageBroker<SwarmIntelligenceJob> broker, ITaskRepository taskRepository, ILoggerFactory loggerFactory)
        {
            _broker = broker;
            _taskRepository = taskRepository;
            _logger = loggerFactory.CreateLogger<SwarmIntelligenceWorker>();
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _broker.Subscribe(async job =>
            {
                _logger.LogInformation("Started job {JobId} at {Time}", job.Id, DateTime.Now);
                try
                {
                    Algorithm algorithm = job.Options.Type switch
                    {
                        SwarmAlgorithm.CanonicalParticleSwarmAlgorithm => new CanonicalParticleSwarmAlgorithm(),
                        SwarmAlgorithm.StandardParticleSwarmAlgorithm => new StandardParticleSwarmAlgorithm(),
                        SwarmAlgorithm.GrayWolfsAlgorithm => new GravitationalSearchAlgorithm(),
                        SwarmAlgorithm.BatGangAlgorithm => new BatGangAlgorithm(),
                        SwarmAlgorithm.GravitationalSearchAlgorithm => new GravitationalSearchAlgorithm(),
                        _ => throw new ArgumentOutOfRangeException()
                    };

                    var problem = new Problem
                    {
                        Function = FunctionFactory.GetFunction(job.Expression),
                        Bounds = job.Bounds,
                    };

                    var start = DateTime.Now;
                    var solution = algorithm.Solve(problem, job.Options, job.LogType);
                    var end = DateTime.Now;
                    await _taskRepository.UpdateSolution(solution, job.Id);
                    _logger.LogInformation("Finished job {JobId} at {Time} in {ExecutionTime}", job.Id, DateTime.Now, end - start);
                }
                catch (Exception e)
                {
                    _logger.LogError("Job {JobId} failed: {Message}", job.Id, e.Message);
                    await _taskRepository.UpdateSolutionStatus(job.Id, JobStatus.Failed, e.Message);
                }
            });
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _broker.Dispose();
            return Task.CompletedTask;
        }
    }
}
