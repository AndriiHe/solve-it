using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SolveIt.Common.MessageQueue;
using SolveIt.Common.MessageQueue.Interfaces;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Options;
using SolveIt.Persistence.Context;
using SolveIt.Persistence.Context.Interfaces;
using SolveIt.Persistence.Repositories;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Worker
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args).ConfigureServices((hostContext, services) =>
            {
                services.Configure<DbOptions>(new ConfigurationBuilder().AddEnvironmentVariables("DB_").Build());
                services.Configure<BrokerOptions>(new ConfigurationBuilder().AddEnvironmentVariables("MQ_").Build());
                services.AddSingleton<IMessageBroker<SwarmIntelligenceJob>, MessageBroker<SwarmIntelligenceJob>>();
                services.AddSingleton<IDbContext, DbContext>();
                services.AddScoped<ITaskRepository, TaskRepository>();
                services.AddHostedService<SwarmIntelligenceWorker>();
            });
        }
    }
}
