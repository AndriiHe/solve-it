using System;
using System.Collections.Generic;
using NUnit.Framework;
using SolveIt.MathParser;

namespace MathParser.Tests
{
    public class LatexExpressionParserTest
    {
        [Test]
        [TestCase(@"1+1", "1+1")]
        [TestCase(@"1-1", "1-1")]
        [TestCase(@"\sum x", "sum(x)")]
        [TestCase(@"\sum x+1", "sum(x)+1")]
        [TestCase(@"2\cdot2", "2*2")]
        [TestCase(@"\frac{\left(\frac{5}{5}\right)}{5}", "(5/5)/5")]
        [TestCase(@"\sum_{i=1}^dx_i^2", "sum(i,1,d,x[i]^2)")]
        [TestCase(@"\prod_{i=1}^dx_i^2", "prod(i,1,d,x[i]^2)")]
        [TestCase(@"\sqrt{\pi^2}", "sqrt(pi^2)")]
        [TestCase(@"\sum_{i=1}^d\sqrt{x_i^2}", "sum(i,1,d,sqrt(x[i]^2))")]
        [TestCase(@"\left(\frac{1}{500}+\sum_{j=1}^{25}\left(\frac{1}{j+\sum_{i=1}^2\left(x_i-a_{i,j}\right)^6}\right)\right)^{-1}", "(1/500+sum(j,1,25,1/(j+sum(i,1,2,(x[i]-a[i,j])^6))))^(-1)")]
        [TestCase(@"\left(\frac{1}{500}+\sum_{j=\frac{1}{1}}^{25}\left(\frac{1}{j+\sum_{i=1}^2\left(x_i-a_{i,j}\right)^6}\right)\right)^{-1}", "(1/500+sum(j,1/1,25,1/(j+sum(i,1,2,(x[i]-a[i,j])^6))))^-1")]
        [TestCase(@"\sum_{i=1}^{d-1}x_i", "sum(i,1,d-1,x[i])")]
        [TestCase(@"\left(\sum_{i=1}^dx_i\right)+\left(\prod_{i=1}^dx_i\right)", "sum(i,1,d,x[i])+prod(i,1,d,x[i])")]
        [TestCase(@"\sum_{i=1}^dx_{\frac{1}{1}}", "sum(i,1,d,x[1/1])")]
        [TestCase(@"\sum_{i=1}^d\left|x_i+\left|i\right|\right|", "sum(i,1,d,abs(x[i]+abs(i)))")]
        [TestCase(@"\sum_{i=1}^{d-1}\left(100\cdot\left(x_{i+1}-x_i^2\right)^2+\left(1-x_i\right)^2\right)", "sum(i,1,d-1,100*(x[i+1]-x[i]^2)^2+(1-x[i])^2)")]
        [TestCase(@"\sum_{i=1}^d\left|x_i\cdot\sin\left(x_i\right)+0.1\cdot x_i\right|", "sum(i,1,d,abs(x[i]*sin(x[i])+0.1*x[i]))")]
        [TestCase(@"-\frac{1}{d}\cdot\sum_{i=1}^d\left(x_i\cdot\sin\left(\sqrt{\left|x_i\right|}\right)\right)+418.983", "-(1/d)*sum(i,1,d,x[i]*sin(sqrt(abs(x[i]))))+418.983")]
        [TestCase(@"\sum_{i=1}^d\left(\left(10^6\right)^{\frac{i-1}{d-1}}\cdot x_i^2\right)", "sum(i,1,d,(10^6)^((i-1)/(d-1))*x[i]^2)")]
        [TestCase(@"\sum _{i=1}^d\left(x_i^2-10\cdot \cos \left(2\cdot \pi \cdot x_i\right)+10\right)", "sum(i,1,d,x[i]^2-10*cos(2*pi*x[i])+10)")]
        [TestCase(@"1+\frac{1}{4000}\cdot\sum_{i=1}^dx_i^2-\prod_{i=1}^d\cos\left(\frac{x_i}{\sqrt{i}}\right)", "1/4000*sum(i,1,d,x[i]^2)-prod(i,1,d,cos(x[i]/sqrt(i)))+1")]
        [TestCase(@"\sum_{i=1}^d-1", "sum(i,1,d,-1)")]
        [TestCase(@"\sum_{d=2}^{3}x[d]", "sum(d,2,3,x[d])")]
        [TestCase(@"\prod_{j=1}^d\sum_{i=1}^5i\cdot\cos\left(\left(i+1\right)\cdot x_j+i\right)", "prod(j,1,d,sum(i,1,5,i*cos((i+1)*x[j]+i)))")]
        [TestCase(@"\sum_{i=1}^d\left|x_i\right|+\prod_{i=1}^d\left|x_i\right|", "sum(i,1,d,abs(x[i]))+prod(i,1,d,abs(x[i]))")]
        [TestCase(@"-20\cdot exp\left(-0.2\cdot\sqrt{\frac{1}{d}\cdot\sum_{i=1}^dx_i^2}\right)-exp\left(\frac{1}{d}\cdot\sum_{i=1}^d\cos\left(2\cdot\pi\cdot x_i\right)\right)+20+e", "-20*exp(-0.2*sqrt(1/d*sum(i,1,d,x[i]^2)))-exp(1/d*sum(i,1,d,cos(2*pi*x[i])))+20+e")]
        [TestCase(@"\frac{1}{d}\cdot\sum_{i=1}^d\left(\sum_{k=0}^{20}\left(0.5^k\cdot\cos\left(2\cdot\pi\cdot3^k\cdot\left(x_i+0.5\right)\right)\right)\right)-\sum_{k=0}^{20}\left(0.5^k\cdot\cos\left(\pi\cdot3^k\right)\right)", "(1/d)*sum(i,1,d,sum(k,0,20,(0.5^k)*cos(2*pi*(3^k)*(x[i]+0.5))))-sum(k,0,20,(0.5^k)*cos(pi*3^k))")]
        // [TestCase(@"", "")]
        public void Should_Convert_Latex_Expression(string latex, string expected)
        {
            var context = new ExpressionContext();
            var values = new[] { 1.0, -2.0, 3.0, 0.5 };
            context.AddMatrix("x", values);
            context.AddVariable("d", values.Length);
            context.AddMatrix("a", new double[2,25]);
            var expectedExpression = new Expression(expected);
            var latexExpression = new Expression(LatexExpressionParser.Parse(latex));

            Assert.That(latexExpression.Execute(context), Is.EqualTo(expectedExpression.Execute(context)).Within(1e-10));
        }

        [Test]
        [TestCase(@"\sum_{i=1}^d\left(\sum_{j=1}^ix_j\right)^2", new []{ -10.0, 10.0 }, new []{ -10.0, 10.0 })]
        [TestCase(@"\sum _{i=1}^d\left(x_i^2-10\cdot \cos \left(2\cdot \pi \cdot x_i\right)+10\right)", new []{ -5.0, 5.0 }, new []{ -5.0, 5.0 })]
        [TestCase(@"\sum_{i=1}^d\left|x_i\cdot\sin\left(x_i\right)+0.1\cdot x_i\right|", new []{ -10.0, 10.0 }, new []{ -10.0, 10.0 })]
        [TestCase(@"\sum_{i=1}^dx_i^2", new []{ -20.0, 20.0 }, new []{ -20.0, 20.0 })]
        [TestCase(@"-0.0001\cdot\left(\left|\sin\left(x_1\right)\cdot\sin\left(x_2\right)\cdot exp\left(\left|100-\frac{\sqrt{x_1^2+x_2^2}}{\pi}\right|\right)\right|+1\right)^{0.1}", new []{ -10.0, 10.0 }, new []{ -10.0, 10.0 })]
        [TestCase(@"\left(1+\left(x_1+x_2+1\right)^2\cdot\left(19-14\cdot x_1+3\cdot x_1^2-14\cdot x_2+6\cdot x_1\cdot x_2+3\cdot x_2^2\right)\right)\cdot\left(30+\left(2\cdot x_1-3\cdot x_2\right)^2\cdot\left(18-32\cdot x_1+12\cdot x_1^2+48\cdot x_2-36\cdot x_1\cdot x_2+27\cdot x_2^2\right)\right)", new []{ -2.0, 2.0 }, new []{ -2.0, 2.0 })]
        [TestCase(@"\sum_{i=1}^dx_i^2+\left(\sum_{i=1}^d0.5\cdot i\cdot x_i\right)^2+\left(\sum_{i=1}^d0.5\cdot i\cdot x_i\right)^4", new []{ -5.0, 10.0 }, new []{ -5.0, 10.0 })]
        [TestCase(@"\sum_{i=1}^{d-1}\left(-\frac{1+\cos\left(12\cdot\sqrt{x_i^2+x_{i+1}^2}\right)}{0.5\cdot\left(x_i^2+x_{i+1}^2\right)+2}\right)", new []{ -5.12, 5.12 }, new []{ -5.12, 5.12 })]
        [TestCase(@"\sum_{i=1}^{d-1}-\left|\sin\left(x_i\right)\cdot\cos\left(x_{i+1}\right)\cdot exp\left(\left|1-\frac{\sqrt{x_i^2+x_{i+1}^2}}{\pi}\right|\right)\right|", new []{ -10.0, 10.0 }, new []{ -10.0, 10.0 })]
        [TestCase(@"\sum_{i=1}^dx_i^3", new []{ -100.0, 100.0 }, new []{ -100.0, 100.0 })]
        [TestCase(@"\sum _{i=1}^{d-1}\left(100\cdot \sqrt{\left|x_{i+1}-0.01\cdot x_i^2\right|}+0.01\cdot \left|x_i+10\right|\right)", new []{ -15.0, -5 }, new []{ -3.0, 3.0 })]
        [TestCase(@"\sum_{i=1}^d\left|x_i\right|+\prod_{i=1}^d\left|x_i\right|", new []{ -10.0, 10.0 }, new []{ -10.0, 10.0 })]
        [TestCase(@"\frac{1}{d}\cdot\sum_{i=1}^d\left(\sum_{k=0}^{20}\left(0.5^k\cdot\cos\left(2\cdot\pi\cdot3^k\cdot\left(x_i+0.5\right)\right)\right)\right)-\sum_{k=0}^{20}\left(0.5^k\cdot\cos\left(\pi\cdot3^k\right)\right)", new []{ -0.5, 0.5 }, new []{ -0.5, 0.5 })]
        [TestCase(@"\sum_{i=1}^d\left(\left(10^6\right)^{\frac{i-1}{d-1}}\cdot x_i^2\right)", new []{ -20.0, 20.0 }, new []{ -20.0, 20.0 })]
        [TestCase(@"1+\frac{1}{4000}\cdot\sum_{i=1}^dx_i^2-\prod_{i=1}^d\cos\left(\frac{x_i}{\sqrt{i}}\right)", new []{ -100.0, 100.0 }, new []{ -100.0, 100.0 })]
        [TestCase(@"\sum_{i=1}^{d-1}\left(100\cdot\left(x_{i+1}-x_i^2\right)^2+\left(1-x_i\right)^2\right)", new []{ -10.0, 10.0 }, new []{ -10.0, 10.0 })]
        [TestCase(@"-\frac{1}{d}\cdot\sum_{i=1}^d\left(x_i\cdot\sin\left(\sqrt{\left|x_i\right|}\right)\right)+418.983", new []{ -500.0, 500.0 }, new []{ -500.0, 500.0 })]
        [TestCase(@"-20\cdot exp\left(-0.2\cdot\sqrt{\frac{1}{d}\cdot\sum_{i=1}^dx_i^2}\right)-exp\left(\frac{1}{d}\cdot\sum_{i=1}^d\cos\left(2\cdot\pi\cdot x_i\right)\right)+20+e", new []{ -30.0, 30.0 }, new []{ -30.0, 30.0 })]
        public void Should_Calculate_Surface_By_Latex(string latex, double[] boundsX, double[] boundsY)
        {
            var latexExpression = new Expression(LatexExpressionParser.Parse(latex));

            var x = new List<List<double>>();
            var y = new List<List<double>>();
            var z = new List<List<double>>();
            for (var x_i = boundsX[0]; x_i < boundsX[1]; x_i += (boundsX[1] - boundsX[0]) / 50)
            {
                var row_x = new List<double>();
                var row_y = new List<double>();
                var row_z = new List<double>();
                for (var y_i = boundsY[0]; y_i < boundsY[1]; y_i += (boundsY[1] - boundsY[0]) / 50)
                {
                    var values = new[] { x_i, y_i };
                    var context = new ExpressionContext();
                    context.AddMatrix("x", values);
                    context.AddVariable("d", values.Length);
                    row_x.Add(x_i);
                    row_y.Add(y_i);
                    row_z.Add(latexExpression.Execute(context));
                }
                x.Add(row_x);
                y.Add(row_y);
                z.Add(row_z);
            }

            Console.WriteLine("var x = [");
            x.ForEach(r =>
            {
                Console.WriteLine($"  [{string.Join(',', r)}],");
            });
            Console.WriteLine("];");

            Console.WriteLine("var y = [");
            y.ForEach(r =>
            {
                Console.WriteLine($"  [{string.Join(',', r)}],");
            });
            Console.WriteLine("];");

            Console.WriteLine("var z = [");
            z.ForEach(r =>
            {
                Console.WriteLine($"  [{string.Join(',', r)}],");
            });
            Console.WriteLine("];");


            // Assert.That(latexExpression.Execute(context), Is.EqualTo(expectedExpression.Execute(context)).Within(1e-10));
        }
    }
}
