using NUnit.Framework;
using SolveIt.MathParser;

namespace MathParser.Tests
{
    public class ExpressionTest
    {
        [Test]
        [TestCase("sum(i,1,d,(sum(j,1,d,(j+0.5)*((x[j]^i)-1/(j^i))))^2)", 0, new []{1, 0.5})]
        [TestCase("sum(i, 1, d, x[i]^2) + sum(i, 1, d, 0.5 * i * x[i])^2 + sum(i, 1, d, 0.5 * i * x[i])^4", 0, new []{0.0, 0.0})]
        [TestCase("1 + 1", 2, new double[0])]
        [TestCase("length(x)", 0, new double[0])]
        [TestCase("length(x)", 3, new []{ 0.5, 2, 2.5})]
        [TestCase("length(a)", 9, new double[0])]
        [TestCase("1+-1", 0, new double[0])]
        [TestCase("1-+1", 0, new double[0])]
        [TestCase("1-(-1)", 2, new double[0])]
        [TestCase("--1", 1, new double[0])]
        [TestCase("----1", 1, new double[0])]
        [TestCase("++1", 1, new double[0])]
        [TestCase("----1", 1, new double[0])]
        [TestCase("--1--1*-1", 0, new double[0])]
        [TestCase("---1---1*-1", 0, new double[0])]
        [TestCase("---1---1*---1", 0, new double[0])]
        [TestCase("--1--1*--1", 2, new double[0])]
        [TestCase("-1--1*-1", -2, new double[0])]
        [TestCase("1--1/1", 2, new double[0])]
        [TestCase("-1-(-1)", 0, new double[0])]
        [TestCase("sum(i, 1, d, x[i])", 5, new []{ 0.5, 2, 2.5})]
        [TestCase("sum(a)", 45, new []{ 0.5, 2, 2.5})]
        [TestCase("sum(d,2,3,x[d])", 10.5, new []{ 0.5, 2, 2.5})]
        [TestCase("sum(x)", 5, new []{ 0.5, 2, 2.5})]
        [TestCase("max(x)", 2.5, new []{ 0.5, 2, 2.5})]
        [TestCase("min(x)", 0.5, new []{ 0.5, 2, 2.5})]
        [TestCase("min(x, 1)", 0.5, new []{ 0.5, 2, 2.5})]
        [TestCase("max(i, 1, d, x[i])", 2.5, new []{ 0.5, 2, 2.5})]
        [TestCase("max(i, 1, d, abs(x[i]))", 2.5, new []{ -0.5, 2, -2.5})]
        [TestCase("max(1,2,3)", 3, new []{ 0.5, 2, 2.5})]
        [TestCase("max(min(max(1,2),max(2,3)),min(max(4,5),max(5,6)),min(max(7,8),max(8,9)))", 8, new []{ 0.5, 2, 2.5})]
        [TestCase("sum(i, 1, d, sum(j, 1, d, sum(k, 1, d, sum(o, 1, d, sum(l, 1, d, sum(n, 1, d, sum(m, 1, d, x[m])))))))", 3645, new []{ 0.5, 2, 2.5})]
        [TestCase("max(i, 1, d, max(j, 1, d, x[j]+x[i]))", 5, new []{ 0.5, 2, 2.5})]
        [TestCase("max(d, 1, d, max(j, 1, d, x[j]))", 5, new []{ 0.5, 5, 2.5})]
        [TestCase("min(i, 1, d, abs(x[i]))", 0.5, new []{ 0.5, -5, 2.5})]
        [TestCase("min(2*d, 1, -d, x[d])", -3, new []{ 0.5, -5, 2.5})]
        [TestCase("max(x[1], x[2], x[3], -1, sin(1)) + min(1,2,5,12,1)", 3.5, new []{ 0.5, 2, 2.5})]
        [TestCase("sum(i,1,d,sum(j,1,d,a[i,j]))", 45, new []{ 0.5, 2, 2.5})]
        [TestCase("prod(j,1,2,sum(i,1,2,i+j))", 35, new []{ 0.5, 2, 2.5})]
        [TestCase("(1/d)*sum(i,1,d,sum(k,0,20,(0.5^k)*cos(2*pi*(3^k)*(x[i]+0.5))))-sum(k,0,20,(0.5^k)*cos(pi*3^k))", 1.3333326975504558, new []{ 5.0, 2.5, 5.0})]
        public void Should_Calculate_Expression_Value(string expression, double expected, double[] x)
        {
            var context = new ExpressionContext();
            context.AddMatrix("x", x);
            context.AddMatrix("a", new double[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } });
            context.AddVariable("d", x.Length);
            Assert.AreEqual(expected, new Expression(expression).Execute(context));
        }
        //
        // [TestCase("g1(x) + d;x[] = { 1 2 3 4 5 }; g1(y) = sum(y); d = length(x)", 20)]
        // public void Should_Calculate_Expression_Value_With_User_Defined_Context(string expression, double expected)
        // {
        //     Assert.AreEqual(expected, new Expression(expression).Execute(new ExpressionContext()));
        // }
    }
}
