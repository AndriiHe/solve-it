using System;
using System.Threading.Tasks;

namespace SolveIt.Common.MessageQueue.Interfaces
{
    public interface IMessageBroker<T> : IDisposable
    {
        void Publish(T message);
        void Subscribe(Func<T, Task> onMessage);
    }
}
