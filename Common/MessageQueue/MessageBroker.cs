using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SolveIt.Common.MessageQueue.Interfaces;
using SolveIt.Common.Model.Options;

namespace SolveIt.Common.MessageQueue
{
    public class MessageBroker<T> : IMessageBroker<T>
    {
        private readonly IModel _channel;
        private readonly IConnection _connection;
        private readonly BrokerOptions _options;
        private readonly IBasicProperties _properties;

        public MessageBroker(IOptions<BrokerOptions> options)
        {
            _options = options.Value;
            var factory = new ConnectionFactory
            {
                HostName = _options.HostName,
                Port = _options.Port,
                UserName = _options.UserName,
                Password = _options.Password
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(_options.Queue, true, false, false, null);
            _channel.BasicQos(0, 1, false);
            _properties = _channel.CreateBasicProperties();
            _properties.Persistent = true;
        }

        public void Publish(T message)
        {
            var data = JsonConvert.SerializeObject(message, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
            _channel.BasicPublish("", _options.Queue, _properties, Encoding.UTF8.GetBytes(data));
        }

        public void Subscribe(Func<T, Task> onMessage)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (model, args) =>
            {
                try
                {
                    var message = Encoding.UTF8.GetString(args.Body.ToArray());
                    await onMessage(JsonConvert.DeserializeObject<T>(message));
                    _channel.BasicAck(args.DeliveryTag, false);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    _channel.BasicReject(args.DeliveryTag, true);
                }
            };
            _channel.BasicConsume(_options.Queue, false, consumer);
        }

        public void Dispose()
        {
            _channel.Dispose();
            _connection.Dispose();
        }
    }
}
