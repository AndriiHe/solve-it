using System.Collections.Generic;

namespace SolveIt.Common.Model.Charts
{
    public class ChartData
    {
        public IEnumerable<IEnumerable<double>> X;
        public IEnumerable<IEnumerable<double>> Y;
        public IEnumerable<IEnumerable<double>> Z;

        public ChartData(IEnumerable<IEnumerable<double>> x, IEnumerable<IEnumerable<double>> y, IEnumerable<IEnumerable<double>> z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
