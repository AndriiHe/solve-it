using System.Collections.Generic;

namespace SolveIt.Common.Model.Charts
{
    public class ChartOptions
    {
        public string Expression;
        public IList<Range> Bounds;
        public int Resolution;
    }
}
