using System;
using System.Collections.Generic;
using SolveIt.Common.Model.Algorithms.Options;

namespace SolveIt.Common.Model
{
    public class AlgorithmJob
    {
        public Guid Id { get; set; }
        public AlgorithmOptions Options { get; set; }
        public IEnumerable<Solution> Solutions { get; set; }
    }
}
