using System;
using System.Collections.Generic;

namespace SolveIt.Common.Model
{
    public class SwarmIntelligenceTask
    {
        public Guid Id { get; set; }
        public string Expression { get; set; }
        public string Latex { get; set; }
        public Range[] Bounds { get; set; }
        public List<AlgorithmJob> Algorithms { get; set; }
        public LogProcess LogProcess { get; set; }
    }
}
