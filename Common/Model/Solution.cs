using System;
using System.Collections.Generic;
using System.Linq;

namespace SolveIt.Common.Model
{
    public class Solution
    {
        public Guid Id;
        public double Value;
        public double[] Position;
        public int IterationsCount;
        public double? Accuracy;
        public List<double> BestValueLog;
        public List<double[]> BestPositionLog;
        public List<double[][]> PositionLog;

        public Solution() {}

        public Solution(SearchAgent searchAgent, IEnumerable<SearchAgent> population, LogProcess logType)
        {
            var shouldLogPosition = searchAgent.Position.Length == 2 && logType == LogProcess.Full;
            var shouldLogBestPosition = logType == LogProcess.Full || logType == LogProcess.Base || logType == LogProcess.Position;
            var shouldLogBestValue = logType == LogProcess.Full || logType == LogProcess.Base || logType == LogProcess.Value;
            IterationsCount = 0;
            Accuracy = null;
            Position = searchAgent.Position;
            Value = searchAgent.Value;
            BestValueLog = shouldLogBestValue ? new List<double> { searchAgent.Value } : null;
            BestPositionLog = shouldLogBestPosition ? new List<double[]> { searchAgent.Position } : null;
            PositionLog = shouldLogPosition ? new List<double[][]> { population.Select(i => i.Position).ToArray() } : null;
        }

        public Solution(SearchAgent searchAgent, double[][] positions, LogProcess logType)
        {
            var shouldLogPosition = searchAgent.Position.Length == 2 && logType == LogProcess.Full;
            var shouldLogBestPosition = logType == LogProcess.Full || logType == LogProcess.Base || logType == LogProcess.Position;
            var shouldLogBestValue = logType == LogProcess.Full || logType == LogProcess.Base || logType == LogProcess.Value;
            IterationsCount = 0;
            Accuracy = null;
            Position = searchAgent.Position;
            Value = searchAgent.Value;
            BestValueLog = shouldLogBestValue ? new List<double> { searchAgent.Value } : null;
            BestPositionLog = shouldLogBestPosition ? new List<double[]> { searchAgent.Position } : null;
            PositionLog = shouldLogPosition ? new List<double[][]> { positions } : null;
        }

        public void Update(SearchAgent searchAgent, IEnumerable<SearchAgent> population)
        {
            Update(searchAgent, population.Select(i => i.Position).ToArray());
        }

        public void Update(SearchAgent searchAgent, double[][] positions)
        {
            IterationsCount++;
            Accuracy = Math.Abs(Value - searchAgent.Value);
            Position = searchAgent.Position;
            Value = searchAgent.Value;
            BestValueLog?.Add(searchAgent.Value);
            BestPositionLog?.Add(searchAgent.Position);
            PositionLog?.Add(positions);
        }
    }
}
