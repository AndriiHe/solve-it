using JsonSubTypes;
using Newtonsoft.Json;

namespace SolveIt.Common.Model.Algorithms.Options
{
    [JsonConverter(typeof(JsonSubtypes), "Type")]
    [JsonSubtypes.KnownSubType(typeof(BatGangOptions), SwarmAlgorithm.BatGangAlgorithm)]
    [JsonSubtypes.KnownSubType(typeof(CanonicalParticleSwarmOptions), SwarmAlgorithm.CanonicalParticleSwarmAlgorithm)]
    [JsonSubtypes.KnownSubType(typeof(StandardParticleSwarmOptions), SwarmAlgorithm.StandardParticleSwarmAlgorithm)]
    [JsonSubtypes.KnownSubType(typeof(GravitationalSearchOptions), SwarmAlgorithm.GravitationalSearchAlgorithm)]
    [JsonSubtypes.KnownSubType(typeof(GrayWolvesOptions), SwarmAlgorithm.GrayWolfsAlgorithm)]
    public class AlgorithmOptions
    {
        public readonly int AgentNumber;
        public int RepeatNumber { get; set; }
        public SwarmAlgorithm Type { get; set; }
        public int MaxIterationNumber;
        public double Accuracy;

        public AlgorithmOptions()
        {
        }

        public AlgorithmOptions(int agentNumber, int maxIteration, double accuracy)
        {
            AgentNumber = agentNumber;
            MaxIterationNumber = maxIteration;
            Accuracy = accuracy;
        }
    }
}
