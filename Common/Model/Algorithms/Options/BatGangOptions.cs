namespace SolveIt.Common.Model.Algorithms.Options
{
    public class BatGangOptions : AlgorithmOptions
    {
        public double Alpha;
        public double Gamma;
        public Range InitialPulseRate;
        public Range InitialLoudness;
        public Range Frequency;

        public BatGangOptions(int agentNumber, int maxIteration, double accuracy) : base(agentNumber, maxIteration, accuracy)
        {
        }
    }
}
