namespace SolveIt.Common.Model.Algorithms.Options
{
    public class CanonicalParticleSwarmOptions : AlgorithmOptions
    {
        public double InertialCoefficient;
        public double CognitiveCoefficient;
        public double SocialCoefficient;

        public CanonicalParticleSwarmOptions(int agentNumber, int maxIteration, double accuracy) : base(agentNumber, maxIteration, accuracy)
        {
        }
    }
}
