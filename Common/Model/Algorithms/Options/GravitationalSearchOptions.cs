namespace SolveIt.Common.Model.Algorithms.Options
{
    public class GravitationalSearchOptions : AlgorithmOptions
    {
        public GravitationalSearchOptions(int agentNumber, int maxIteration, double accuracy) : base(agentNumber, maxIteration, accuracy)
        {
        }
    }
}
