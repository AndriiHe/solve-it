namespace SolveIt.Common.Model.Algorithms.Options
{
    public class StandardParticleSwarmOptions : AlgorithmOptions
    {
        public Range InertialWeight;
        public double CognitiveCoefficient;
        public double SocialCoefficient;

        public StandardParticleSwarmOptions(int agentNumber, int maxIteration, double accuracy) : base(agentNumber, maxIteration, accuracy)
        {
        }
    }
}
