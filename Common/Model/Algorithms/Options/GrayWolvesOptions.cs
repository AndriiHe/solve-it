namespace SolveIt.Common.Model.Algorithms.Options
{
    public class GrayWolvesOptions : AlgorithmOptions
    {
        public GrayWolvesOptions(int agentNumber, int maxIteration, double accuracy) : base(agentNumber, maxIteration, accuracy)
        {
        }
    }
}
