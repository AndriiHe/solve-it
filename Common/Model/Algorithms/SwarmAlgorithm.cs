namespace SolveIt.Common.Model.Algorithms
{
    public enum SwarmAlgorithm
    {
        CanonicalParticleSwarmAlgorithm = 1,
        GrayWolfsAlgorithm = 2,
        BatGangAlgorithm = 3,
        GravitationalSearchAlgorithm = 4,
        StandardParticleSwarmAlgorithm = 5,
    }
}
