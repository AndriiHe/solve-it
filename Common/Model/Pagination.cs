namespace SolveIt.Common.Model
{
    public class Pagination
    {
        public int Page { get; set; }
        public int PerPage { get; set; }
        public int TotalPages { get; set; }
        public int TotalItems { get; set; }
    }
}
