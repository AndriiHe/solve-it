namespace SolveIt.Common.Model.Options
{
    public class DbOptions
    {
        public string ConnectionString { get; set; }
    }
}
