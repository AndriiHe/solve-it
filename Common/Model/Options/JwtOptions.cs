namespace SolveIt.Common.Model.Options
{
    public class JwtOptions
    {
        public string Issuer { get; set; }
        public string Key { get; set; }
        public int ExpiresIn { get; set; }
        public string Salt { get; set; }
    }
}
