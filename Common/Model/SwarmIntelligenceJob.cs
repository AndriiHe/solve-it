using System;
using SolveIt.Common.Model.Algorithms.Options;

namespace SolveIt.Common.Model
{
    public class SwarmIntelligenceJob
    {
        public Guid Id { get; set; }
        public LogProcess LogType { get; set; }
        public AlgorithmOptions Options { get; set; }
        public string Expression { get; set; }
        public Range[] Bounds { get; set; }
    }
}
