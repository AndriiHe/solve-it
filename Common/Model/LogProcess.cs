namespace SolveIt.Common.Model
{
    public enum LogProcess
    {
        None = 0,
        Base = 1,
        Full = 2,
        Position = 3,
        Value = 4
    }
}
