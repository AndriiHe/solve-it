using System;
using System.Linq;

namespace SolveIt.Common.Model
{
    public class SearchAgent
    {
        public readonly double[] Position;
        public readonly double Value;

        public SearchAgent(double[] position, double value)
        {
            Position = position;
            Value = value;
        }

        public bool Equals(SearchAgent other)
        {
            return Position.SequenceEqual(other.Position) && Value.Equals(other.Value);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Position, Value);
        }
    }
}
