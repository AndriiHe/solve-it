namespace SolveIt.Common.Model
{
    public class Range
    {
        public readonly double Min;
        public readonly double Max;

        public Range(double min, double max)
        {
            Min = min;
            Max = max;
        }
    }
}
