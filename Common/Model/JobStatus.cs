namespace SolveIt.Common.Model
{
    public enum JobStatus
    {
        NotStarted = 1,
        Done = 2,
        Failed = 3,
    }
}
