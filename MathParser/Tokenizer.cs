using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SolveIt.MathParser.Model;

namespace SolveIt.MathParser
{
    public static class Tokenizer
    {
        private const string Parameters = @"(\(|\[)(?>(\(|\[)(?<c>)|[^()\[\]]+|(\)|\])(?<-c>))*(?(c)(?!))(\)|\])";
        private static readonly Regex LiteralPattern = new Regex(@"([\w])+");
        private static readonly Regex DigitsPattern = new Regex(@"([\d.])+");
        private static readonly Regex OperatorsPattern = new Regex(@"(\+|\-|\*|\/|\^)");
        private static readonly Regex ParenthesisPattern = new Regex(@"([\(\)\[\]])");
        private static readonly Regex ExpressionPartsPattern = new Regex(@"([\w.])+|([^\s])", RegexOptions.IgnoreCase);

        private static readonly Dictionary<string, FunctionData> Functions = new Dictionary<string, FunctionData>
        {
            { "sum", new FunctionData("", "+") },
            { "prod", new FunctionData("", "*") },
            { "max", new FunctionData("max", ",") },
            { "min", new FunctionData("min", ",") },
        };

        public static IEnumerable<Token> Tokenize(string expression, ExpressionContext context)
        {
            var tokens = GetTokens(expression, context);
            return ToPostfix(tokens, context);
        }

        private static IEnumerable<Token> GetTokens(string expression, ExpressionContext context)
        {
            return PreProcess(expression, context).Matches(ExpressionPartsPattern).Select(ConvertToToken);
        }

        private static IEnumerable<Token> ToPostfix(IEnumerable<Token> tokens, ExpressionContext context)
        {
            var operators = context.GetOperators();
            var stack = new Stack<Token>();
            foreach (var token in tokens)
            {
                switch (token.Type)
                {
                    case TokenType.Number:
                    case TokenType.Variable:
                        yield return token;
                        break;
                    case TokenType.Function:
                    case TokenType.Array:
                        stack.Push(token);
                        break;
                    case TokenType.Comma:
                        while (!stack.Peek().IsOpenParenthesis()) yield return stack.Pop();
                        break;
                    case TokenType.Operator:
                        while (stack.Count > 0 && stack.Peek().IsOperator() && operators[token.Value].HasLowerPriority(operators[stack.Peek().Value]))
                            yield return stack.Pop();
                        stack.Push(token);
                        break;
                    case TokenType.Parenthesis when token.IsOpenParenthesis():
                        stack.Push(token);
                        break;
                    case TokenType.Parenthesis:
                        while (!stack.Peek().IsOpenParenthesis()) yield return stack.Pop();
                        stack.Pop();
                        if (stack.Count > 0 && stack.Peek().IsParameterized()) yield return stack.Pop();
                        break;
                    default:
                        throw new Exception("Wrong token");
                }
            }

            while (stack.Count > 0) yield return stack.Pop();
        }

        private static int GetParametersNumber(string expression)
        {
            return expression.Match($"^\\w*{Parameters}").Matches($"(?<=([\\[(,]))(([_a-z0-9-+\\/\\*\\^\\.]*)({Parameters})*)*(?=([\\]),]))").Count();
        }

        private static Token ConvertToToken(string part, int index, IEnumerable<string> parts)
        {
            var token = parts.ToList();
            var nextPart = token.ElementAtOrDefault(index + 1);
            var expression = token.Skip(index + 1).Take(token.Count - index + 1).Join("");
            if (LiteralPattern.IsMatch(part) && nextPart == "[")
                return new Token(TokenType.Array, part, GetParametersNumber(expression));
            if (LiteralPattern.IsMatch(part) && nextPart == "(")
                return new Token(TokenType.Function, part, GetParametersNumber(expression));
            if (DigitsPattern.IsMatch(part))
                return new Token(TokenType.Number, part);
            if (LiteralPattern.IsMatch(part))
                return new Token(TokenType.Variable, part);
            if (part == ",")
                return new Token(TokenType.Comma, part);
            if (ParenthesisPattern.IsMatch(part))
                return new Token(TokenType.Parenthesis, part);
            if (OperatorsPattern.IsMatch(part))
                return new Token(TokenType.Operator, part, 2);

            throw new ArgumentException("Unknown token type");
        }

        private static string PreProcess(string input, ExpressionContext context)
        {
            var normalizedExpression = input.ToLower()
                .Replace(" ", "")
                .Replace("(-", "(0-")
                .Replace(",-", ",0-")
                .Replace("()", "(0)")
                .Replace("--", "+")
                .Replace("+-", "-")
                .Replace("++", "+")
                .ReplaceAll(@"(?<=(\^))(-\w*)", "(0$2)");
            var matrices = normalizedExpression.Matches(@"([\w.])+(?!\[)").Where(context.IsMatrixExists);
            var expression = matrices.Aggregate(normalizedExpression, (accumulator, part) => Regex.Replace(accumulator, $"(?<!([a-z0-9_])){part}(?!([a-z0-9_]))", UnwrapMatrices(part, context)));
            var functions = Functions.Keys
                .SelectMany(function => ExtractFunctions(expression, function))
                .OrderBy(function => expression.IndexOf(function, StringComparison.Ordinal));
            return functions.Aggregate(expression, (accumulator, function) => UnwrapFunctions(accumulator, function, context));
        }

        private static string UnwrapMatrices(string part, ExpressionContext context)
        {
            var (rows, columns) = context.GetMatrixSize(part);
            return Enumerable.Range(1, rows)
                .SelectMany(i => Enumerable.Range(1, columns).Select(j => $"{part}[{i},{j}]"))
                .Join(",");
        }

        private static IEnumerable<string> ExtractFunctions(string expression, string name)
        {
            return expression.Matches($"{name}{Parameters}");
        }

        private static string UnwrapFunctions(string expression, string part, ExpressionContext context)
        {
            var name = part.Match(@"\w*(?=\()");
            var parameters = part.Matches($"(?<=([(,]))(([_a-z0-9-+\\/\\*\\^\\.]*)({Parameters})*)*(?=([),]))").ToList();

            var isRepeated = parameters.Count == 4 && !DigitsPattern.IsMatch(parameters[0]) && LiteralPattern.IsMatch(parameters[0]) && !context.IsVariableExist(parameters[0]);

            if (!isRepeated)
            {
                return parameters.Where(parameter => !string.IsNullOrEmpty(parameter))
                    .Aggregate(expression, (exp, parameter) => exp.Replace(parameter, PreProcess(parameter, context)));

            }
            var variable = parameters[0];
            var start = (int) new Expression(parameters[1]).Execute(context);
            var end = (int) new Expression(parameters[2]).Execute(context);
            var value = parameters[3];

            // TODO: add check end >= start

            var unwrapped = Enumerable.Repeat(0, end - start + 1)
                .Select((_, index) => PreProcess(Regex.Replace(value, $"(?<!([a-z0-9_])){variable}(?!([a-z0-9_]))", $"{start + index}"),context));

            var function = Functions.GetValueOrDefault(name);

            return expression.Replace(part, $"{function?.Prefix}({unwrapped.Join(function?.Separator)})");
        }
    }

    internal class FunctionData
    {
        public readonly string Prefix;
        public readonly string Separator;

        public FunctionData(string prefix, string separator)
        {
            Prefix = prefix;
            Separator = separator;
        }
    }
}
