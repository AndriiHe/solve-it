using System;
using System.Collections.Generic;
using System.Linq;

namespace SolveIt.MathParser
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TSource>, TResult> selector)
        {
            var sourceList = source.ToList();
            return sourceList.Select((value, index) => selector(value, index, sourceList));
        }

        public static string Join<TSource>(this IEnumerable<TSource> source, string separator)
        {
            return string.Join(separator, source);
        }
    }
}
