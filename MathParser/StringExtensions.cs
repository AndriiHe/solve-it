using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace SolveIt.MathParser
{
    public static class StringExtensions
    {
        public static string ReplaceAll(this string input, string pattern, string replacement)
        {
            while (Regex.IsMatch(input, pattern))
                input = Regex.Replace(input, pattern, replacement);

            return input;
        }

        public static IEnumerable<string> Matches(this string input, Regex regex)
        {
            return regex.Matches(input).Select(m => m.Value);
        }

        public static IEnumerable<string> Matches(this string input, string pattern)
        {
            return Regex.Matches(input, pattern).Select(m => m.Value);
        }

        public static string Match(this string input, string pattern)
        {
            return Regex.Match(input, pattern).Value;
        }
    }
}
