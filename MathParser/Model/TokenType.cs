namespace SolveIt.MathParser.Model
{
    public enum TokenType
    {
        Number,
        Variable,
        Function,
        Array,
        Parenthesis,
        Operator,
        Comma
    }
}
