using System;

namespace SolveIt.MathParser.Model
{
    public class Function
    {
        public string Name { get; set; }
        public Func<double[], double> Execute { get; set; }

        public Function(string name, Func<double[], double> execute)
        {
            Name = name;
            Execute = execute;
        }
    }
}
