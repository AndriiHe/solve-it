namespace SolveIt.MathParser.Model
{
    public readonly struct Token
    {
        public TokenType Type { get; }
        public string Value { get; }
        public int ArgumentsNumber { get; }

        public Token(TokenType type, string value)
        {
            Type = type;
            Value = value;
            ArgumentsNumber = 0;
        }

        public Token(TokenType type, string value, int argumentsNumber)
        {
            Type = type;
            Value = value;
            ArgumentsNumber = argumentsNumber;
        }

        public bool IsOpenParenthesis()
        {
            return Value == "(" || Value == "[";
        }

        public bool IsParameterized()
        {
            return Type == TokenType.Array || Type == TokenType.Function;
        }

        public bool IsOperator()
        {
            return Type == TokenType.Operator;
        }

        public Token WithArguments(int argumentsNumber)
        {
            return new Token(Type, Value, argumentsNumber);
        }
    }
}
