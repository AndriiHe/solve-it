using System;

namespace SolveIt.MathParser.Model
{
    public class Operator
    {
        private string Name { get; set; }
        private int Precedence { get; set; }
        private bool RightAssociative { get; set; }
        public Func<double[], double> Apply { get; set; }

        public Operator(string name, int precedence, bool rightAssociative, Func<double[], double> apply)
        {
            Name = name;
            Precedence = precedence;
            RightAssociative = rightAssociative;
            Apply = apply;
        }

        public bool HasLowerPriority(Operator o)
        {
            return RightAssociative ? Precedence < o.Precedence : Precedence <= o.Precedence;
        }
    }
}
