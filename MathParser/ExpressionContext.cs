using System;
using System.Collections.Generic;
using System.Linq;
using SolveIt.MathParser.Model;

namespace SolveIt.MathParser
{
    public class ExpressionContext
    {
        private readonly Dictionary<string, Operator> _operators = new Dictionary<string, Operator>
        {
            { "+", new Operator("+", 2, false, AddValues) },
            { "-", new Operator("-", 2, false, SubtractValues) },
            { "*", new Operator("*", 3, false, MultiplyValues) },
            { "/", new Operator("/", 3, false, DivideValues) },
            { "^", new Operator("^", 4, true, ExponentValues) },
        };

        private readonly Dictionary<string, Function> _functions = new Dictionary<string, Function>
        {
            { "sin", new Function("sin", parameters => Math.Sin(parameters[0])) },
            { "cos", new Function("cos", parameters => Math.Cos(parameters[0])) },
            { "sqrt", new Function("sqrt", parameters => Math.Sqrt(parameters[0])) },
            { "abs", new Function("abs", parameters => Math.Abs(parameters[0])) },
            { "exp", new Function("exp", parameters => Math.Exp(parameters[0])) },
            { "log", new Function("log", parameters => Math.Log(parameters[0])) },
            { "tan", new Function("tan", parameters => Math.Tan(parameters[0])) },
            { "max", new Function("max", parameters => parameters.Max()) },
            { "min", new Function("min", parameters => parameters.Min()) },
            { "sum", new Function("sum", parameters => parameters.Sum()) },
            { "length", new Function("length", parameters => parameters.Length) },
            { "prod", new Function("prod", parameters => parameters.Aggregate(1.0, (accumulator, i) => accumulator * i)) },
        };

        private readonly Dictionary<string, double[,]> _matrices = new Dictionary<string, double[,]>();
        private readonly Dictionary<string, double> _variables = new Dictionary<string, double>
        {
            { "pi", Math.PI },
            { "e", Math.E },
        };

        public IDictionary<string, Operator> GetOperators() => _operators;

        public double CallOperator(string name, double[] parameters)
        {
            return _operators.GetValueOrDefault(name)?.Apply(parameters) ?? throw new ArgumentException($"Unknown operator {name}");
        }

        public double CallFunction(Token token, double[] parameters)
        {
            return _functions.GetValueOrDefault(token.Value)?.Execute(parameters) ?? throw new ArgumentException($"Unknown function {token.Value}");
        }

        public void AddMatrix(string name, double[] vector)
        {
            var matrix = new double[1, vector.Length];
            for (var i = 0; i < vector.Length; i++) matrix[0, i] = vector[i];
            _matrices[name] = matrix;
        }

        public void AddMatrix(string name, double[,] matrix)
        {
            _matrices[name] = matrix;
        }

        public void AddFunction(Function function)
        {
            _functions.Add(function.Name, function);
        }

        public double GetMatrixItem(Token token, double[] parameters)
        {
            var j = (int) parameters[0] - 1;
            var i = parameters.Length == 2 ? (int) parameters[1] - 1 : 0;
            return _matrices.GetValueOrDefault(token.Value)?[i, j] ?? throw new ArgumentException($"Unknown vector {token.Value}");
        }

        public void AddVariable(string name, double value)
        {
            _variables[name] = value;
        }

        public double GetVariable(string name)
        {
            var isExist = _variables.TryGetValue(name, out var value);
            return isExist ? value : throw new ArgumentException($"Unknown variable {name}");
        }

        public bool IsVariableExist(string name) => _variables.TryGetValue(name, out _);
        public bool IsMatrixExists(string name) => _matrices.TryGetValue(name, out _);

        public Tuple<int, int> GetMatrixSize(string name)
        {
            var isExists = _matrices.TryGetValue(name, out var matrix);
            return isExists ? new Tuple<int, int>(matrix.GetLength(0), matrix.GetLength(1)) : null;
        }

        private static double AddValues(double[] parameters) => parameters.Length == 2 ? parameters[1] + parameters[0] : parameters[0];

        private static double SubtractValues(double[] parameters) => parameters.Length == 2 ? parameters[1] - parameters[0] : -parameters[0];

        private static double MultiplyValues(double[] parameters) => parameters[1] * parameters[0];

        private static double DivideValues(double[] parameters)
        {
            return parameters[1] / parameters[0];
        }

        private static double ExponentValues(double[] parameters)
        {
            return Math.Pow(parameters[1], parameters[0]);
        }
    }
}
