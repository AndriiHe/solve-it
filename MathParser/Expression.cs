using System;
using System.Collections.Generic;
using SolveIt.MathParser.Model;

namespace SolveIt.MathParser
{
    public class Expression
    {
        private readonly string _expression;
        private IEnumerable<Token> _tokens;

        public Expression(string expression)
        {
            _expression = expression;
        }

        public double Execute(ExpressionContext context)
        {
            _tokens ??= Tokenizer.Tokenize(_expression, context);
            var stack = new Stack<double>();
            foreach (var token in _tokens)
            {
                switch (token.Type)
                {
                    case TokenType.Number:
                        stack.Push(double.Parse(token.Value));
                        break;
                    case TokenType.Operator:
                        stack.Push(context.CallOperator(token.Value, stack.Pop(token.ArgumentsNumber)));
                        break;
                    case TokenType.Function:
                        stack.Push(context.CallFunction(token,  stack.Pop(token.ArgumentsNumber)));
                        break;
                    case TokenType.Variable:
                        stack.Push(context.GetVariable(token.Value));
                        break;
                    case TokenType.Array:
                        stack.Push(context.GetMatrixItem(token, stack.Pop(token.ArgumentsNumber)));
                        break;
                    case TokenType.Parenthesis:
                    case TokenType.Comma:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return stack.Pop();
        }
    }
}
