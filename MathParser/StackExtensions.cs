using System.Collections.Generic;
using System.Linq;

namespace SolveIt.MathParser
{
    public static class StackExtensions
    {
        public static T[] Pop<T>(this Stack<T> stack, int argumentsNumber)
        {
            var items = argumentsNumber > stack.Count ? stack.Count : argumentsNumber;
            return Enumerable.Range(0, items).Select(_ => stack.Pop()).ToArray();
        }
    }
}
