namespace SolveIt.MathParser
{
    public class LatexExpressionParser
    {
        private const string IndexParameters = @"\{(?>\{(?<c>)|((\w+)=((?>\{(?<c>)|[^{}]+|\}(?<-c>))*(?(c)(?!))+))|\}(?<-c>))*(?(c)(?!))\}";
        private const string LatexParameters = @"\{(?>\{(?<c>)|[^{}]+|\}(?<-c>))*(?(c)(?!))\}";
        private const string IterativeParameters = @"([^-+{()}]*([({](?>[({](?<c>)|[^(){}]+|[)}](?<-c>))*(?(c)(?!))[)}])*(\^\{(?>(\{)(?<c>)|[^{}]+|(\})(?<-c>))*(?(c)(?!))\})*)";
        private const string AbsParameters = @"(?>\<(?<c>)|[^<>]+|\>(?<-c>))*(?(c)(?!))";
        public static string Parse(string expression)
        {
            return expression
                .Trim()
                .ToLower()
                .Replace(" ", "")
                .ReplaceAll($"\\\\frac({LatexParameters})({LatexParameters})", "$1/$2")
                .ReplaceAll(@"(?<=(_))([^{])", "{$2}")
                .ReplaceAll(@"(?<=(\^))([^{])", "{$2}")
                .ReplaceAll(@",\\", ",")
                .ReplaceAll(@"\\\s", " ")
                .ReplaceAll(@"\\left\(", "(")
                .ReplaceAll(@"\\right\)", ")")
                .ReplaceAll(@"\\left\|", "<")
                .ReplaceAll(@"\\right\|", ">")
                .ReplaceAll(@"\\cdot", "*")
                .ReplaceAll($"\\<({AbsParameters})\\>", "abs($1)")
                .ReplaceAll($"\\\\(sum|prod)_({IndexParameters})\\^({LatexParameters})(-?{IterativeParameters}+)", "$1($4,$5,$6,$7)")
                .ReplaceAll($"\\\\(sum|prod)(-?{IterativeParameters}+)", "$1($2)")
                .ReplaceAll(@"\\(sin|cos|sqrt|exp|log|tan|min|max|pi|e)", "$1")
                .ReplaceAll($"_({LatexParameters})", "[$1]")
                .ReplaceAll(@"\}\]", "]")
                .ReplaceAll(@"\[\{", "[")
                .ReplaceAll(@"\}", ")")
                .ReplaceAll(@"\{", "(");
        }
    }
}
