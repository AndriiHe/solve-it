using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolveIt.Api.Common.Errors;
using SolveIt.Api.Reports.Interfaces;

namespace SolveIt.Api.Reports
{
    [ApiController]
    [Route("api/solutions/{solutionId:guid}")]
    [Authorize]
    public class ReportController : Controller
    {
        private readonly IReportService _reportService;

        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpGet("logs")]
        public async Task<ActionResult> GetLog(Guid solutionId)
        {
            var log = await _reportService.GetLogBySolutionId(solutionId);
            return log == null
                ? throw new RestException(HttpStatusCode.NotFound,$"Log for '{solutionId}' task not found.")
                : Ok(log);
        }

        [HttpGet("report")]
        public async Task<ActionResult> GetSolutionReport(Guid solutionId)
        {
            var sheet = await _reportService.GetSolutionSheet(solutionId);
            return File(
                sheet,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"{solutionId}.xlsx");
        }
    }
}
