using System;
using System.IO;
using System.Threading.Tasks;
using SolveIt.Common.Model;

namespace SolveIt.Api.Reports.Interfaces
{
    public interface IReportService
    {
        public Task<Solution> GetLogBySolutionId(Guid id);
        public Task<MemoryStream> GetSolutionSheet(Guid id);
        public Task<MemoryStream> GetTaskSheet(Guid id, Guid userId);
    }
}
