using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using SolveIt.Api.Reports.Interfaces;
using SolveIt.Common.Model;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Api.Reports
{
    public class ReportService : IReportService
    {
        private readonly ISolutionRepository _solutionRepository;
        private readonly ITaskRepository _taskRepository;

        public ReportService(ISolutionRepository solutionRepository, ITaskRepository taskRepository)
        {
            _solutionRepository = solutionRepository;
            _taskRepository = taskRepository;
        }

        public async Task<Solution> GetLogBySolutionId(Guid id)
        {
            return await _solutionRepository.GetById(id);
        }

        public async Task<MemoryStream> GetSolutionSheet(Guid id)
        {
            var log = await _solutionRepository.GetById(id);
            var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("Result");
            worksheet.Cell(1, 1).Value = "k";
            worksheet.Cell(1, 2).Value = "x";
            worksheet.Cell(1, 3).Value = "f(x)";

            for (var i = 0; i < log.BestPositionLog?.Count; i++)
            {
                worksheet.Cell(i + 2, 1).Value = i;
                worksheet.Cell(i + 2, 2).Value = $"[{string.Join(", ", log.BestPositionLog[i])}]";
            }

            for (var i = 0; i < log.BestValueLog?.Count; i++)
                worksheet.Cell(i + 2, 3).Value = log.BestValueLog[i];

            worksheet.Columns().AdjustToContents();
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            return stream;
        }
        public async Task<MemoryStream> GetTaskSheet(Guid id, Guid userId)
        {
            var task = await _taskRepository.GetById(id, userId);
            var workbook = new XLWorkbook();

            task?.Algorithms.ForEach(job =>
            {
                var sheetName = job.Options.Type.ToString();
                var worksheet = workbook.Worksheets.Add(sheetName[..Math.Min(sheetName.Length, 31)]);
                worksheet.Cell(1, 1).Value = "x";
                worksheet.Cell(1, 2).Value = "f(x)";
                worksheet.Cell(1, 3).Value = "k";
                worksheet.Cell(1, 4).Value = "e";
                var row = 1;

                job.Solutions?.ToList().OrderBy(s => s.Value).ToList().ForEach(solution =>
                {
                    row++;
                    if (solution?.Position != null)
                        worksheet.Cell(row, 1).Value = $"[{string.Join(", ", solution.Position)}]";
                    worksheet.Cell(row, 2).Value = solution?.Value;
                    worksheet.Cell(row, 3).Value = solution?.IterationsCount;
                    worksheet.Cell(row, 4).Value = solution?.Accuracy;
                });

                worksheet.Columns().AdjustToContents();
            });

            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            return stream;
        }
    }
}
