using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace SolveIt.Api
{
    public static class StartupExtensions
    {
        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidIssuer = Environment.GetEnvironmentVariable("JWT_Issuer"),
                ValidAudience = Environment.GetEnvironmentVariable("JWT_Issuer"),
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Environment.GetEnvironmentVariable("JWT_Key") ?? "")),
            };

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(config =>
            {
                config.IncludeErrorDetails = true;
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;
                config.TokenValidationParameters = tokenValidationParameters;
            });
        }
    }
}
