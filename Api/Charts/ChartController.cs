using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolveIt.Common.Model.Charts;

namespace SolveIt.Api.Charts
{
    [ApiController]
    [Route("api/charts")]
    [Authorize]
    public class ChartController : Controller
    {
        private readonly ChartService _chartService;

        public ChartController(ChartService chartService)
        {
            _chartService = chartService;
        }

        [HttpPost]
        public IActionResult Solve(ChartOptions options)
        {
            var data = _chartService.GetData(options);
            return Ok(data);
        }
    }
}
