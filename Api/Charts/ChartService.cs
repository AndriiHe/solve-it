using System;
using System.Collections.Generic;
using SolveIt.Common.Model.Charts;
using SolveIt.MathParser;

namespace SolveIt.Api.Charts
{
    public class ChartService
    {
        public ChartData GetData(ChartOptions options)
        {
            var latexExpression = new Expression(LatexExpressionParser.Parse(options.Expression));
            var context = new ExpressionContext();
            var xStep = (options.Bounds[0].Max - options.Bounds[0].Min) / options.Resolution;
            var yStep = (options.Bounds[1].Max - options.Bounds[1].Min) / options.Resolution;

            var x = new List<List<double>>();
            var y = new List<List<double>>();
            var z = new List<List<double>>();
            for (var i = options.Bounds[0].Min; i <= options.Bounds[0].Max + xStep; i += xStep)
            {
                var rowX = new List<double>();
                var rowY = new List<double>();
                var rowZ = new List<double>();
                for (var j = options.Bounds[1].Min; j <= options.Bounds[1].Max + yStep; j += yStep)
                {
                    var values = new double[] { i, j };
                    context.AddMatrix("x", values);
                    context.AddVariable("d", values.Length);
                    rowX.Add(Math.Round(i, 8));
                    rowY.Add(Math.Round(j, 8));
                    rowZ.Add(Math.Round(latexExpression.Execute(context), 8));
                }
                x.Add(rowX);
                y.Add(rowY);
                z.Add(rowZ);
            }
            return new ChartData(x, y, z);
        }
    }
}
