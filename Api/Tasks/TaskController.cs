using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;
using SolveIt.Api.Common.Errors;
using SolveIt.Api.Reports.Interfaces;
using SolveIt.Api.Tasks.Interfaces;
using SolveIt.Common.Model;

namespace SolveIt.Api.Tasks
{
    [ApiController]
    [Route("api/tasks")]
    [Authorize]
    public class TaskController : Controller
    {
        private readonly ITaskService _taskService;
        private readonly IReportService _reportService;

        public TaskController(ITaskService taskService, IReportService reportService)
        {
            _taskService = taskService;
            _reportService = reportService;
        }

        [HttpPost]
        public async Task<IActionResult> Solve(SwarmIntelligenceTask task)
        {
            var userId = Guid.Parse(User.FindFirstValue(JwtRegisteredClaimNames.Jti));
            var id = await _taskService.Publish(task, userId);
            return Ok(new { Id = id });
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var userId = Guid.Parse(User.FindFirstValue(JwtRegisteredClaimNames.Jti));
            var task = await _taskService.GetById(id, userId);
            return task == null
                ? throw new RestException(HttpStatusCode.NotFound,"Task not found.")
                : Ok(task);
        }

        [HttpGet("{id:guid}/report")]
        public async Task<IActionResult> GetTaskReport(Guid id)
        {
            var userId = Guid.Parse(User.FindFirstValue(JwtRegisteredClaimNames.Jti));
            var sheet = await _reportService.GetTaskSheet(id, userId);
            return File(
                sheet,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"{id}.xlsx");
        }

        [HttpGet]
        public async Task<IActionResult> GetList(int page = 1, int perPage = 10)
        {
            var userId = Guid.Parse(User.FindFirstValue(JwtRegisteredClaimNames.Jti));
            var pagination = await _taskService.GetTasksPagination(page, perPage, userId);
            var tasks = await _taskService.GetTasks(page, perPage, userId);
            return Ok(new { tasks, pagination });
        }
    }
}
