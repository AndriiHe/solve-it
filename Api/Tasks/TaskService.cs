using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolveIt.Api.Tasks.Interfaces;
using SolveIt.Common.MessageQueue.Interfaces;
using SolveIt.Common.Model;
using SolveIt.MathParser;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Api.Tasks
{
    public class TaskService : ITaskService
    {
        private readonly IMessageBroker<SwarmIntelligenceJob> _messageBroker;
        private readonly ITaskRepository _taskRepository;

        public TaskService(IMessageBroker<SwarmIntelligenceJob> messageBroker, ITaskRepository taskRepository)
        {
            _messageBroker = messageBroker;
            _taskRepository = taskRepository;
        }

        public Task<Guid> Solve(SwarmIntelligenceTask task)
        {
            throw new NotImplementedException();
        }

        public async Task<Guid> Publish(SwarmIntelligenceTask task, Guid userId)
        {
            task.Expression = LatexExpressionParser.Parse(task.Latex);
            var taskId = await _taskRepository.Create(task, userId);
            var jobs = await _taskRepository.GetTaskJobs(taskId);
            jobs.ToList().ForEach(job => _messageBroker.Publish(job));
            return taskId;
        }

        public async Task<SwarmIntelligenceTask> GetById(Guid id, Guid userId)
        {
            return await _taskRepository.GetById(id, userId);
        }

        public async Task<IEnumerable<SwarmIntelligenceTask>> GetTasks(int page, int perPage, Guid userId)
        {
            return await _taskRepository.GetTasks(page, perPage, userId);
        }

        public async Task<Pagination> GetTasksPagination(int page, int perPage, Guid userId)
        {
            var totalItems = await _taskRepository.GetTasksPagination(userId);
            var pagination = new Pagination
            {
                Page = page,
                PerPage = perPage,
                TotalItems = totalItems,
                TotalPages = (int)Math.Ceiling((double)totalItems / perPage)
            };
            return pagination;
        }
    }
}
