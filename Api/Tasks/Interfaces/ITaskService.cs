using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SolveIt.Common.Model;

namespace SolveIt.Api.Tasks.Interfaces
{
    public interface ITaskService
    {
        public Task<Guid> Solve(SwarmIntelligenceTask task);
        public Task<Guid> Publish(SwarmIntelligenceTask task, Guid userId);
        public Task<SwarmIntelligenceTask> GetById(Guid id, Guid userid);
        public Task<IEnumerable<SwarmIntelligenceTask>> GetTasks(int page, int perPage, Guid userId);
        public Task<Pagination> GetTasksPagination(int page, int perPage, Guid userId);
    }
}
