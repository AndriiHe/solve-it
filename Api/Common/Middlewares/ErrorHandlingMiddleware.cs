using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SolveIt.Api.Common.Errors;

namespace SolveIt.Api.Common.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<ErrorHandlingMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                _logger.LogInformation(exception, "{Message}", exception.Message);
                await HandleExceptionAsync(context, exception);
            }
        }

        private static async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var restException = exception as RestException;
            var error = new
            {
                message = exception.Message,
                errors = restException?.Errors,
                timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
            };
            var result = JsonConvert.SerializeObject(error);

            context.Response.StatusCode = (int)(restException?.Code ?? HttpStatusCode.InternalServerError);
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(result);
        }
    }
}
