using System;
using System.Collections.Generic;
using System.Net;

namespace SolveIt.Api.Common.Errors
{
    public class RestException : Exception
    {
        public HttpStatusCode Code { get; }
        public Dictionary<string, string> Errors { get; }

        public RestException(HttpStatusCode code, Dictionary<string, string> errors, string message = null) : base(message)
        {
            Code = code;
            Errors = errors;
        }
        public RestException(HttpStatusCode code, string message = null) : base(message)
        {
            Code = code;
        }
    }
}
