using System.Linq;
using System.Net;
using FluentValidation;
using FluentValidation.AspNetCore;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using SolveIt.Api.Common.Errors;

namespace SolveIt.Api.Common.Interceptors
{
    public class ValidationInterceptor: IValidatorInterceptor
    {
        public IValidationContext BeforeAspNetValidation(ActionContext actionContext, IValidationContext commonContext)
        {
            return commonContext;
        }

        public ValidationResult AfterAspNetValidation(ActionContext actionContext, IValidationContext validationContext, ValidationResult result)
        {
            var errors = result.Errors?
                .ToLookup(e => e.PropertyName, e => e.ErrorMessage)
                .ToDictionary(e => e.Key, e => e.First());

            return result.IsValid
                ? result
                : throw new RestException(HttpStatusCode.BadRequest, errors, "Validation error occured");
        }
    }
}
