using FluentValidation;
using SolveIt.Common.Model;

namespace SolveIt.Api.Common.Validators
{
    public class RangeValidator: AbstractValidator<Range>
    {
        public RangeValidator()
        {
            RuleFor(value => value.Min).LessThan(value => value.Max);
            RuleFor(value => value.Max).NotEmpty();
            RuleFor(value => value.Min).NotEmpty();
        }
    }
}
