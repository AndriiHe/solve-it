using FluentValidation;
using SolveIt.Common.Model;

namespace SolveIt.Api.Common.Validators
{
    public class UserValidator: AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(user => user.Username).NotEmpty().EmailAddress();
            RuleFor(user => user.Password).NotEmpty().Length(8, 100);
        }
    }
}
