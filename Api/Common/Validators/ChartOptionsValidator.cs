using FluentValidation;
using SolveIt.Common.Model.Charts;

namespace SolveIt.Api.Common.Validators
{
    public class ChartOptionsValidator: AbstractValidator<ChartOptions>
    {
        public ChartOptionsValidator()
        {
            RuleFor(options => options.Expression).NotEmpty().SetValidator(new ExpressionValidator<ChartOptions, string>());
            RuleFor(options => options.Bounds).NotNull()
                .Must(bounds => bounds == null || bounds.Count == 2)
                .WithMessage("'Bounds' length must be equals 2");
            RuleForEach(options => options.Bounds).NotNull().SetValidator(new RangeValidator());
            RuleFor(options => options.Resolution).NotNull().GreaterThanOrEqualTo(10).LessThanOrEqualTo(50);
        }
    }
}
