using System;
using FluentValidation;
using FluentValidation.Validators;
using SolveIt.MathParser;

namespace SolveIt.Api.Common.Validators
{
    public class ExpressionValidator<T, TProperty> : PropertyValidator<T, TProperty>
    {
        public override string Name => "IsValidExpression";

        public override bool IsValid(ValidationContext<T> context, TProperty value)
        {
            try
            {
                var equation = LatexExpressionParser.Parse(value?.ToString() ?? "");
                var ctx = new ExpressionContext();
                var x = new [] { 1.0, 1.0 };
                ctx.AddMatrix("x", x);
                ctx.AddVariable("d", x.Length);
                new Expression(equation).Execute(ctx);
                return true;
            }
            catch (Exception e)
            {
                context.MessageFormatter.AppendArgument("Error", e.Message);
                return false;
            }
        }

        protected override string GetDefaultMessageTemplate(string errorCode) => "'{PropertyName}' is invalid LaTex expression. {Error}";
    }
}
