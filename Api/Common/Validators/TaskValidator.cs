using FluentValidation;
using SolveIt.Common.Model;

namespace SolveIt.Api.Common.Validators
{
    public class TaskValidator : AbstractValidator<SwarmIntelligenceTask>
    {
        public TaskValidator()
        {
            RuleFor(task => task.Latex).NotEmpty();
        }
    }
}
