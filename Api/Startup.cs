using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SolveIt.Api.Auth;
using SolveIt.Api.Auth.Interfaces;
using SolveIt.Api.Charts;
using SolveIt.Api.Common.Interceptors;
using SolveIt.Api.Common.Middlewares;
using SolveIt.Api.Reports;
using SolveIt.Api.Reports.Interfaces;
using SolveIt.Api.Tasks;
using SolveIt.Api.Tasks.Interfaces;
using SolveIt.Common.MessageQueue;
using SolveIt.Common.MessageQueue.Interfaces;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Options;
using SolveIt.Persistence.Context;
using SolveIt.Persistence.Context.Interfaces;
using SolveIt.Persistence.Repositories;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                options.SerializerSettings.Formatting = Formatting.Indented;
            }).AddFluentValidation();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });
            services.AddJwtAuthentication();
            services.AddResponseCompression();
            services.AddValidatorsFromAssemblyContaining<Startup>(ServiceLifetime.Transient);
            services.Configure<DbOptions>(new ConfigurationBuilder().AddEnvironmentVariables("DB_").Build());
            services.Configure<JwtOptions>(new ConfigurationBuilder().AddEnvironmentVariables("JWT_").Build());
            services.Configure<BrokerOptions>(new ConfigurationBuilder().AddEnvironmentVariables("MQ_").Build());
            services.AddSingleton<IMessageBroker<SwarmIntelligenceJob>, MessageBroker<SwarmIntelligenceJob>>();
            services.AddSingleton<IDbContext, DbContext>();
            services.AddScoped<ITaskRepository, TaskRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ISolutionRepository, SolutionRepository>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ChartService, ChartService>();
            services.AddTransient<IValidatorInterceptor, ValidationInterceptor>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            app.UseMiddleware<RequestLoggingMiddleware>()
                .UseMiddleware<ErrorHandlingMiddleware>()
                .UseRouting()
                .UseCors("AllowAll")
                .UseAuthentication()
                .UseAuthorization()
                .UseResponseCompression()
                .UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
