using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Npgsql;
using SolveIt.Api.Auth.Interfaces;
using SolveIt.Api.Common.Errors;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Options;
using SolveIt.Persistence.Repositories.Interfaces;

namespace SolveIt.Api.Auth
{
    public class AuthService : IAuthService
    {
        private readonly JwtOptions _options;
        private readonly IUserRepository _userRepository;

        public AuthService(IOptions<JwtOptions> options, IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _options = options.Value;
        }

        public async Task<string> Authenticate(Credentials credentials)
        {
            var user = await _userRepository.GetUserByEmail(credentials.Username);
            if (user == null) return null;
            var isMatched = Hash(credentials.Password, _options.Salt) == user.Password;
            return !isMatched ? null : GenerateToken(user);
        }

        public async Task<Guid> Register(User user)
        {
            var isExists = await _userRepository.IsUserExists(user.Username);

            return isExists
                ? throw new ConflictException()
                : await _userRepository.CreateUser(new User { Password = Hash(user.Password, _options.Salt), Username = user.Username });
        }

        public string GenerateToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString()),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.Key));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(_options.ExpiresIn);

            var token = new JwtSecurityToken(
                _options.Issuer,
                _options.Issuer,
                claims,
                expires: expires,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private static string Hash(string value, string salt)
        {;
            return string.Join("", Hash(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(salt)).Select(b => b.ToString("x2")));
        }

        private static IEnumerable<byte> Hash(IEnumerable<byte> value, IEnumerable<byte> salt)
        {
            var saltedValue = value.Concat(salt).ToArray();
            return new SHA256Managed().ComputeHash(saltedValue);
        }
    }
}
