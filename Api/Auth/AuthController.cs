using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolveIt.Api.Auth.Interfaces;
using SolveIt.Api.Common.Errors;
using SolveIt.Common.Model;

namespace SolveIt.Api.Auth
{
    [ApiController]
    [Route("api")]
    public class AuthController : Controller
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] Credentials credentials)
        {
            var token = await _authService.Authenticate(credentials);
            return token == null
                ? throw new RestException(HttpStatusCode.Unauthorized, "Invalid email or password")
                : Ok(new { Token = token });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] User user)
        {
            try
            {
                var id = await _authService.Register(user);
                return Ok(new { Id = id });
            }
            catch (ConflictException)
            {
                var errors = new Dictionary<string, string> { { "Username", "Username already taken" } };
                throw new RestException(HttpStatusCode.Conflict, errors, "User already registered");
            }
        }
    }
}
