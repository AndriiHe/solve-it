using System;
using System.Threading.Tasks;
using SolveIt.Common.Model;

namespace SolveIt.Api.Auth.Interfaces
{
    public interface IAuthService
    {
        Task<string> Authenticate(Credentials credentials);
        Task<Guid> Register(User user);
    }
}
