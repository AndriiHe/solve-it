export const USER_TOKEN = 'token';
export const ROUTES = {
  ROOT: '/',
  LOGIN: '/login',
  REGISTER: '/register',
  CREATE_TASKS: '/tasks/new',
  LIST_TASKS: '/tasks',
}
