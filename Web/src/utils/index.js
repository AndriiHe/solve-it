import decode from 'jwt-decode';

export const decodeUserToken = token => {
  const payload = token && decode(token);
  return payload && { username: payload.sub };
};
