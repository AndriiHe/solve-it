import React, { useEffect } from 'react';
import styles from './App.module.scss';
import { Breadcrumb, Layout } from 'antd'
import Header from './components/Header/Header';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import SideMenu from './components/SideMenu';
import CreateTaskPage from './containers/CreateTaskPage';
import NotFoundPage from './containers/NotFoundPage';
import LoginPage from './containers/LoginPage';
import { USER_TOKEN, ROUTES } from './constants';
import { setUser } from './store/actions/user';
import { useDispatch, useSelector } from 'react-redux';
import { decodeUserToken } from './utils';

const App = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.data);

  const token = localStorage.getItem(USER_TOKEN);
  useEffect(() => dispatch(setUser(decodeUserToken(token))), [token, dispatch]);

  const layout = (
    <Layout className={ styles.app }>
      <Header user={ user }/>
      <Layout>
        <SideMenu/>
        <Layout.Content className={ styles.layout }>
          <Breadcrumb className={ styles.breadcrumbs }>
            <Breadcrumb.Item>Task</Breadcrumb.Item>
            <Breadcrumb.Item>12345</Breadcrumb.Item>
          </Breadcrumb>
          <div className={ styles.content }>
            <Switch>
              <Route exact path={ ROUTES.ROOT }><Redirect to={ ROUTES.LIST_TASKS }/></Route>
              <Route exact path={ ROUTES.CREATE_TASKS } component={ CreateTaskPage }/>
              <Route exact path={ ROUTES.LIST_TASKS } component={ CreateTaskPage }/>
              <Route path={ ROUTES.ROOT } component={ NotFoundPage }/>
            </Switch>
          </div>
        </Layout.Content>
      </Layout>
    </Layout>
  );

  const login = (
    <Switch>
      <Route exact path={ ROUTES.LOGIN } component={ LoginPage }/>
      <Route exact path={ ROUTES.REGISTER } component={ LoginPage }/>
      <Redirect to={ ROUTES.LOGIN } />
    </Switch>
  );

  return (
    <Router>
      { user || token ? layout : login }
    </Router>
  );
}

export default App;
