import { createReducer } from '@reduxjs/toolkit';
import { setUser } from '../actions/user';

const initialState = {
  data: undefined
};

export default createReducer(initialState, {
  [setUser.type]: (state, action) => ({ ...state, data: action.payload })
})
