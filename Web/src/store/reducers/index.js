import charts from './charts';
import user from './user';
import { combineReducers } from 'redux';
import { userLogout } from '../actions/user';

const reducer = combineReducers({
  charts,
  user,
});

const rootReducer = (state, action) => action.type === userLogout.type ? reducer(undefined, action) : reducer(state, action);

export default rootReducer;
