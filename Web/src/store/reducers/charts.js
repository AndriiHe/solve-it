import { createReducer } from '@reduxjs/toolkit';
import { getChartData } from '../actions/charts';

const initialState = {
  errors: undefined,
  isLoading: false,
  data: undefined
};

export default createReducer(initialState, {
  [getChartData.pending]: (state, action) => ({ isLoading: true, data: undefined, errors: undefined }),
  [getChartData.fulfilled]: (state, action) => ({ ...state, isLoading: false, data: action.payload, errors: undefined }),
  [getChartData.rejected]: (state, action) => ({ ...state, isLoading: false, data: initialState.data, errors: action.error.message }),
})
