import { createAction } from '@reduxjs/toolkit';
import { USER_LOGOUT, SET_USER } from '../constants/user';

export const userLogout = createAction(USER_LOGOUT);
export const setUser = createAction(SET_USER);
