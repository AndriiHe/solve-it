import { createAsyncThunk } from '@reduxjs/toolkit';
import { GET_CHART_DATA } from '../constants/charts';
import api from '../../api';

export const getChartData = createAsyncThunk(GET_CHART_DATA, async (task) => {
  const bounds = task.isHypercube ? new Array(task.boundsNumber).fill(task.bounds && task.bounds[0]) : task.bounds && task.bounds.slice(0, 2);
  return await api.getChartData(task.expression, bounds, 50);
});
