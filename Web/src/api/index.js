import { getChartData } from './charts';
import { login, register as registerUser } from './user';

const api = {
  getChartData,
  login,
  registerUser,
};

export default api;
