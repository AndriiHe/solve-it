import axios from 'axios';

const httpClient = axios.create({
  baseURL: process.env.REACT_APP_API_URI
});

httpClient.interceptors.request.use((config) => {
  const token = localStorage.getItem('token');
  config.headers.Authorization = `Bearer ${token}`;

  return config;
});

export default httpClient;
