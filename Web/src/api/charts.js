import httpClient from './httpClient';

export const getChartData = (expression, bounds, resolution) => httpClient.post('/api/charts', { expression, bounds, resolution }).then(({ data }) => data);
