import httpClient from './httpClient';

export const login = (credentials) => httpClient.post('/api/login', credentials).then(({ data }) => data);
export const register = (user) => httpClient.post('/api/register', user).then(({ data }) => data);
