import i18n from 'i18next';
import en from './en.json';
import uk from './uk.json';
import { initReactI18next } from 'react-i18next';
import detector from 'i18next-browser-languagedetector';

const resources = { en, uk };

i18n
  .use(detector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: 'uk',
    supportedLngs: ['uk', 'en'],
    interpolation: {
      escapeValue: false,
    },
    debug: true
  });

export default i18n;
