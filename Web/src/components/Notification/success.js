import { notification } from 'antd';
import { SmileOutlined } from '@ant-design/icons';

const success = (message, { t }) => {
  notification.open({
    message: t && t('common:notifications.success'),
    description: message,
    icon: <SmileOutlined style={{ color: '#3bc90e' }} />,
  });
};

export default success;
