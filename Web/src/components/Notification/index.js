import success from './success';
import fail from './fail';

const notification = {
  success,
  fail,
};

export default notification;
