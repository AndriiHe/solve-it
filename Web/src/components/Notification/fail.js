import { notification } from 'antd';
import { SmileOutlined } from '@ant-design/icons';

const fail = (message, { t }) => {
  notification.open({
    message: t && t('common:notifications.fail'),
    description: message,
    icon: <SmileOutlined style={{ color: '#fc0f2b' }} />,
  });
};

export default fail;
