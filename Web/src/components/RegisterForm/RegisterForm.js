import Form from '../Form/Form';
import Input from '../Form/Fields/Input';
import { Button } from 'antd';
import React from 'react';
import * as yup from 'yup';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

const RegisterForm = ({ onSubmit, loading }) => {
  const { t } = useTranslation();

  const registerSchema = yup.object().shape({
    username: yup.string()
      .required(t('common:login.form.validation.username.required'))
      .email(t('common:login.form.validation.username.email')),
    password: yup.string()
      .required(t('common:login.form.validation.password.required'))
      .min(8, t('common:login.form.validation.password.length')),
    passwordConfirmation: yup.string()
      .required(t('common:login.form.validation.passwordConfirmation.required'))
      .equals([yup.ref('password')], t('common:login.form.validation.passwordConfirmation.equals'))
  });

  return (
    <Form onSubmit={ onSubmit } validationSchema={ registerSchema } initialValues={ { username: '', password: '' } }>
      <Input
        name='username'
        autoComplete='username'
        placeholder={ t('common:login.form.fields.username') }
        prefix={ <UserOutlined/> }
      />
      <Input
        name='password'
        type='password'
        autoComplete='current-password'
        placeholder={ t('common:login.form.fields.password') }
        prefix={ <LockOutlined/> }
      />
      <Input
        name='passwordConfirmation'
        type='password'
        autoComplete='current-password'
        placeholder={ t('common:login.form.fields.passwordConfirmation') }
        prefix={ <LockOutlined/> }
      />
      <Button htmlType='submit' style={ { width: '100%', marginTop: '0.5rem' } } type='primary' loading={ loading }
              disabled={ loading }>
        { t('common:login.form.registerButton') }
      </Button>
    </Form>
  );
};

export default RegisterForm;
