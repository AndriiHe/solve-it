import EquationEditor from 'equation-editor-react';
import React from 'react';
import { Dropdown, Menu } from "antd";
import { DownOutlined } from '@ant-design/icons';
import { useTranslation } from "react-i18next";

const ExpressionEditor = ({ value, onChange, placeholder, functions }) => {
  const { t } = useTranslation();
  const menu = functions && (
    <Menu onClick={ onChange }>
      { functions.map(({ name, value }) => (<Menu.Item key={ value }>{ t(name) }</Menu.Item>)) }
    </Menu>
  );

  return (
    <div className='expression-editor'>
      <EquationEditor
        value={ value }
        onChange={ onChange }
        autoCommands='pi sqrt sum prod'
        autoOperatorNames='sin cos tan exp max min log'
      />
      <div className={ `function ${ value ? '' : 'empty' }` }>f(x)=</div>
      { !value && (<div className='placeholder'>{ placeholder }</div>) }
      { functions && (<Dropdown className='functions-selector' overlay={ menu } placement='bottomLeft'><DownOutlined/></Dropdown>) }
    </div>
  );
}

export default ExpressionEditor;
