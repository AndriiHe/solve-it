import Icon from '@ant-design/icons';
import { ReactComponent as EnSvg } from './en.svg';

const LanguageEnIcon = () => <Icon component={ EnSvg }/>;

export default LanguageEnIcon;
