import styles from './Header.module.scss';
import { Button, Dropdown, Layout, Menu } from 'antd';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import LanguageUkIcon from './LanguageUkIcon';
import LanguageEnIcon from './LanguageEnIcon';
import { UserOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { userLogout } from '../../store/actions/user';
import { ROUTES, USER_TOKEN } from '../../constants';
import { useHistory } from 'react-router-dom';

const Header = ({ user }) => {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();

  const Language = useMemo(() => ({ 'uk': LanguageUkIcon, 'en': LanguageEnIcon }[i18n.language]), [i18n.language]);

  const languagesMenu = (
    <Menu onClick={ ({ key }) => i18n.changeLanguage(key) }>
      <Menu.Item key='uk' icon={ <LanguageUkIcon/> }>
        <span className={ styles.languageLabel }>{ t('common:header.language.uk') }</span>
      </Menu.Item>
      <Menu.Item key='en' icon={ <LanguageEnIcon/> }>
        <span className={ styles.languageLabel }>{ t('common:header.language.en') }</span>
      </Menu.Item>
    </Menu>
  );

  const logOut = () => {
    localStorage.removeItem(USER_TOKEN);
    dispatch(userLogout());
    history.push(ROUTES.LOGIN);
  };

  const userMenu = (
    <Menu>
      <Menu.Item key='logout' onClick={ logOut }>
        { t('common:header.logout') }
      </Menu.Item>
    </Menu>
  );

  return (
    <Layout.Header className={ styles.header }>
      <div style={ { display: 'flex', alignItems: 'center', justifyContent: 'space-between' } }>
        <div className={ styles.logo }>Solve It</div>
        <div className={ styles.user }>
          <Dropdown overlay={ languagesMenu }>
            <Button type='text' shape='circle' icon={ <Language/> } className={ styles.headerButton }/>
          </Dropdown>
          <div className={ styles.username }>{ user && user.username }</div>
          <Dropdown overlay={ userMenu }>
            <Button shape='circle' icon={ <UserOutlined/> } className={ styles.headerButton }/>
          </Dropdown>
        </div>
      </div>
    </Layout.Header>
  );
};

export default Header;
