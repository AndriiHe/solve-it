import Icon from '@ant-design/icons';
import { ReactComponent as UkSvg } from './uk.svg';

const LanguageUkIcon = () => <Icon component={ UkSvg }/>;

export default LanguageUkIcon;

