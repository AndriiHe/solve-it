import React from 'react';
import { Form, Checkbox as AntdCheckbox } from 'antd';
import { Field } from 'formik';

const status = {
  ERROR: 'error',
  SUCCESS: 'success'
};

const Checkbox = ({ name, label, required, className, type, ...props }) => (
  <Field name={ name }>
    {
      ({ field, form: { errors, touched, submitCount } }) => {
        const error = (submitCount || touched[name]) && (errors[name] || props.error);
        const validationStatus = error ? status.ERROR : status.SUCCESS;
        return (
          <Form.Item className={ className } help={ error } validateStatus={ validationStatus } required={ required }>
            <AntdCheckbox
              name={ name }
              label={ label }
              checked={ field.value }
              { ...field }
              { ...props }
            >
              { label }
            </AntdCheckbox>
          </Form.Item>
        )
      }
    }
  </Field>
);

export default Checkbox;
