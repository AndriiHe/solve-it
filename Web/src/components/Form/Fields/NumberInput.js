import React from 'react';
import { Form, InputNumber } from 'antd';
import { Field } from 'formik';

const status = {
  ERROR: 'error',
  SUCCESS: 'success'
};

const Input = ({ name, label, required, className, ...props }) => (
  <Field name={ name }>
    {
      ({ field, form: { errors, touched, submitCount, setFieldValue, setFieldTouched } }) => {
        const error = (submitCount || touched[name]) && (errors[name] || props.error);
        const validationStatus = error ? status.ERROR : status.SUCCESS;
        return (
          <Form.Item className={ className } label={ label } help={ error } validateStatus={ validationStatus } required={ required}>
            <InputNumber
              name={ name }
              { ...props }
              value={ field.value }
              onChange={ value => {
                setFieldValue(name, value);
                setFieldTouched(name, true);
              } }
            />
          </Form.Item>
        )
      }
    }
  </Field>
);

export default Input;
