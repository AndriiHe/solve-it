import React from 'react';
import { Form } from 'antd';
import { Field } from 'formik';
import ExpressionEditor from "../../ExpressionEditor";

const status = {
  ERROR: 'error',
  SUCCESS: 'success'
};

const ExpressionInput = ({ name, label, className, placeholder, required, functions, ...props }) => (
  <Field name={ name }>
    {
      ({ field, form: { errors, touched, submitCount, setFieldValue, setFieldTouched } }) => {
        const error = (submitCount || touched[name]) && (errors[name] || props.error);
        const validationStatus = error ? status.ERROR : status.SUCCESS;
        return (
          <Form.Item className={ className } label={ label } help={ error } validateStatus={ validationStatus } required={ required }>
            <ExpressionEditor
              functions={ functions }
              placeholder={ placeholder }
              value={ field.value }
              onChange={ value => {
                setFieldValue(name, value);
                setFieldTouched(name, true);
              } }
            />
          </Form.Item>
        )
      }
    }
  </Field>
);


export default ExpressionInput;
