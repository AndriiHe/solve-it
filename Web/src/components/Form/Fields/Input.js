import React from 'react';
import { Form, Input as AntdInput } from 'antd';
import { Field } from 'formik';

const status = {
  ERROR: 'error',
  SUCCESS: 'success'
};

const Input = ({ name, label, required, className, type, ...props }) => (
  <Field name={ name }>
    {
      ({ field, form: { errors, touched, submitCount } }) => {
        const error = (submitCount || touched[name]) && (errors[name] || props.error);
        const validationStatus = error ? status.ERROR : status.SUCCESS;
        const TextInput = type && type.toLowerCase() === 'password' ? AntdInput.Password : AntdInput;
        return (
          <Form.Item className={ className } label={ label } help={ error } validateStatus={ validationStatus } required={ required }>
            <TextInput
              name={ name }
              { ...field }
              { ...props }
            />
          </Form.Item>
        )
      }
    }
  </Field>
);

export default Input;
