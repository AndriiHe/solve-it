import React from 'react';
import propTypes from 'prop-types';
import { Formik } from 'formik';
import { Form as AntdForm } from 'antd';

const Form = ({ children, onChange, layoutOptions, layout, ...props }) => {
  return (
    <Formik { ...props } validate={ onChange } validateOnBlur={ false }>
      {
        ({ handleSubmit, resetForm }) => (<AntdForm onFinish={ value => handleSubmit(value, { resetForm }) } layout={ layout } { ...layoutOptions } >{ children }</AntdForm>)
      }
    </Formik>
  );
}

Form.propTypes = { children: propTypes.node };

export default Form;
