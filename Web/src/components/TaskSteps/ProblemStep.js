import ExpressionInput from '../Form/Fields/ExpressionInput';
import styles from './ProblemStep.module.scss';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Checkbox, NumberInput } from '../Form';
import SurfaceChart from "../SurfaceChart";

const ProblemStep = ({ task, chartData, loading }) => {
  const { t } = useTranslation();

  const dimensions = useMemo(() => task.isHypercube ? 1 : task.boundsNumber, [task.isHypercube, task.boundsNumber]);

  const getBoundRow = (number) => (
    <div style={ { display: 'flex' } } key={ number }>
      <NumberInput
        name={ `bounds.${ number }.min` }
        label={ t('common:task.form.fields.minBound') + ` d[${ number + 1 }]` }
        placeholder={ t('common:task.form.fields.minBound') + ` d[${ number + 1 }]` }
        required
      />
      <NumberInput
        name={ `bounds.${ number }.max` }
        label={ t('common:task.form.fields.maxBound') + ` d[${ number + 1 }]` }
        placeholder={ t('common:task.form.fields.maxBound') + ` d[${ number + 1 }]` }
        required
      />
    </div>
  );

  return (
    <>
      <SurfaceChart data={ chartData } loading={ loading } />
      <ExpressionInput
        functions={ [{ value: `sum_{i=1}^dx_i^2`, name: 'Sphere' }] }
        name='expression'
        label={ t('common:task.form.fields.expression') }
        placeholder={ t('common:task.form.fields.expression') }
        required
      />
      <div className={ styles.bounds }>
        <NumberInput
          className={ styles.item }
          min={ 2 }
          name='boundsNumber'
          label={ t('common:task.form.fields.boundsNumber') }
          placeholder={ t('common:task.form.fields.boundsNumber') }
          required
        />
        <Checkbox
          className={ styles.item }
          name='isHypercube'
          label={ t('common:task.form.fields.hypercubeBounds') }
        />
      </div>
      {
        task.isHypercube
          ? getBoundRow(0)
          : new Array(dimensions).fill(0).map((_, i) => getBoundRow(i))
      }
    </>
  );
};

export default ProblemStep;
