import Form from '../Form/Form';
import Input from '../Form/Fields/Input';
import { Button } from 'antd';
import React from 'react';
import * as yup from 'yup';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

const LoginForm = ({ onSubmit, loading }) => {
  const { t } = useTranslation();

  const loginSchema = yup.object().shape({
    username: yup.string().required(t('common:login.form.validation.username.required')),
    password: yup.string().required(t('common:login.form.validation.password.required')),
  });

  return (
    <Form onSubmit={ onSubmit } validationSchema={ loginSchema } initialValues={ { username: '', password: '' } }>
      <Input
        name='username'
        autoComplete='username'
        placeholder={ t('common:login.form.fields.username') }
        prefix={ <UserOutlined/> }
      />
      <Input
        name='password'
        autoComplete='current-password'
        placeholder={ t('common:login.form.fields.password') }
        prefix={ <LockOutlined/> }
        type='password'
      />
      <Button htmlType='submit' style={ { width: '100%', marginTop: '0.5rem' } } type='primary' loading={ loading } disabled={ loading }>
        { t('common:login.form.loginButton') }
      </Button>
    </Form>
  );
};

export default LoginForm;
