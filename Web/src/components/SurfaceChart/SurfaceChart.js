import React from 'react';
import Plot from 'react-plotly.js';
import { Empty, Skeleton } from "antd";

const SurfaceChart = ({ data, layout, loading }) => {

  const chart = (
    <Plot
      data={ [{ ...(data), type: 'surface' }] }
      layout={ {
        width: 288,
        height: 288,
        title: '',
        margin: { l: 0, r: 0, b: 0, t: 0 },
        paper_bgcolor: 'rgba(0,0,0,0)',
        ...layout
      } }
      config={ { displayModeBar: false } }
    />
  );

  return (
    <div style={ { display: 'flex', justifyContent: 'center' } }>
      <div style={ { height: '18rem', width: '18rem', display: 'flex', alignItems: 'center', justifyContent: 'center' } }>
        { loading ? <Skeleton active/> : data ? chart : <Empty /> }
      </div>
    </div>
  )
};

export default SurfaceChart;
