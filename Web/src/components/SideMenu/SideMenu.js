import { Layout, Menu } from 'antd';
import { DesktopOutlined, FileOutlined, PieChartOutlined, TeamOutlined, UserOutlined } from '@ant-design/icons';
import { Link, useLocation } from 'react-router-dom';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

const SideMenu = () => {
  const [collapsed, setCollapsed] = useState(false);
  const location = useLocation();
  const { t } = useTranslation();

  return (
    <Layout.Sider collapsible collapsed={ collapsed } onCollapse={ setCollapsed }>
      <Menu theme='dark' defaultSelectedKeys={ ['1'] } selectedKeys={ [location.pathname] } mode='inline'>
        <Menu.Item key='/tasks/new' icon={ <PieChartOutlined/> }>
          { t('common:menu.createTask') }
          <Link to='/tasks/new'/>
        </Menu.Item>
        <Menu.Item key='/tasks' icon={ <DesktopOutlined/> }>
          { t('common:menu.taskList') }
          <Link to='/tasks'/>
        </Menu.Item>
      </Menu>
    </Layout.Sider>
  );
};

export default SideMenu;
