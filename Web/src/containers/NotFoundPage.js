import { Button, Result } from 'antd';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

const NotFoundPage = () => {
  const { t } = useTranslation();
  const history = useHistory();

  const onBack = () => history.push('/');

  return (
    <Result
      status='404'
      title={ t('common:notFound.title') }
      subTitle={ t('common:notFound.subTitle') }
      extra={ <Button type='primary' onClick={ onBack }>{ t('common:notFound.back') }</Button> }
    />
  );
}

export default NotFoundPage;
