import React, { useEffect, useMemo, useState } from 'react';
import getTaskSchema from './getTaskSchema';
import { Steps } from 'antd';
import { Form } from '../../components/Form';
import { useTranslation } from 'react-i18next';
import { ProblemStep } from '../../components/TaskSteps';
import { getChartData } from '../../store/actions/charts';
import { useDispatch, useSelector } from 'react-redux';
import useDebounce from '../../hooks/useDebounce';

const initialTask = { boundsNumber: 2, isHypercube: true, bounds: [{ min: -10, max: 10}] };

const CreateTaskPage = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const chart = useSelector(state => state.charts.data);
  const isChartLoading = useSelector(state => state.charts.isLoading);
  const [task, setTask] = useState(initialTask);
  const [step, setStep] = useState(0);
  const taskSchema = useMemo(() => getTaskSchema({ t }), [t]);

  const chartDetails = useDebounce(task, 1500);

  useEffect(() => {
    const shouldDraw = step === 0 && chartDetails && chartDetails.bounds && (chartDetails.bounds.length >= 2 || chartDetails.isHypercube) && chartDetails.expression;
    shouldDraw && dispatch(getChartData(chartDetails))
  }, [chartDetails, step, dispatch])

  const step2 = (<div>
    <Form onSubmit={ console.log } initialValues={ { } } layout='vertical'>

    </Form>
  </div>);

  const steps = [
    { description: t('common:task.steps.problemDescription'), component: <ProblemStep task={ task } chartData={ chart } loading={ isChartLoading } /> },
    { description: t('common:task.steps.algorithmDescription'), component: step2 },
    { description: t('common:task.steps.optionsDescription'), component: <div>Step 3</div> },
  ]

  const onSubmit = (task) => {
    console.log('onSubmit', task)
  }

  return (
    <>
      <Steps current={ step } onChange={ setStep }>
        { steps.map(({ description }, i) => (<Steps.Step title={ `${ t('common:task.steps.step') } ${ i + 1 }` } description={ description } key={ i }/>)) }
      </Steps>
      <Form onChange={ setTask } onSubmit={ onSubmit } validationSchema={ taskSchema } initialValues={ initialTask } layout='vertical'>
        <div>
          { steps[step].component }
        </div>
      </Form>
    </>
  );

  // return (
  //   <>
  //     <ExpressionEditor value={ value } onChange={ setValue }/>
  //     <Plot
  //       data={ [
  //         {
  //           ...(data),
  //           type: 'surface',
  //         }
  //       ] }
  //       layout={ {
  //         width: 300,
  //         height: 300,
  //         title: '',
  //         margin: { l: 10, r: 10, b: 0, t: 0 },
  //         paper_bgcolor: 'rgba(0,0,0,0)'
  //       } }
  //       config={ { displayModeBar: false } }
  //     />
  //   </>
  // );
}

export default CreateTaskPage;
