import * as yup from 'yup';

const getTaskSchema = ({ t }) => yup.object().shape({
  expression: yup.string().required(t('common:task.form.validation.expression.required')),
  boundsNumber: yup.number().min(2).integer()
});

export default getTaskSchema;
