import React, { useMemo, useState } from 'react';
import { Tabs } from 'antd';
import { useHistory, useLocation } from 'react-router-dom';
import logo from '../../resources/logo.svg';
import { useTranslation } from 'react-i18next';
import { ROUTES, USER_TOKEN } from '../../constants';
import LoginForm from '../../components/LoginForm';
import api from '../../api';
import RegisterForm from '../../components/RegisterForm';
import { useDispatch } from 'react-redux';
import { setUser } from '../../store/actions/user';
import { decodeUserToken } from '../../utils';
import notification from '../../components/Notification';

const LoginPage = () => {
  const location = useLocation();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState();
  const history = useHistory();
  const { t } = useTranslation();
  const activeTab = useMemo(() => location.pathname.toLowerCase(), [location.pathname]);

  const onTabClick = tab => !loading && history.push(tab);

  const onLogin = (credentials, { resetForm }) => {
    setLoading(true);
    api.login(credentials)
      .then(({ token }) => {
        localStorage.setItem(USER_TOKEN, token);
        resetForm();
        dispatch(setUser(decodeUserToken(token)));
        history.push(ROUTES.ROOT);
      })
      .catch(e => {
        const message = e && e.response && e.response.data && e.response.data.message;
        notification.fail(t(`errors:${message}`), { t });
        setLoading(false);
      });
  };

  const onRegister = (user, { resetForm }) => {
    api.registerUser(user)
      .then(() => {
        notification.success(t('common:login.form.registerMessage'), { t });
        history.push(ROUTES.LOGIN);
        resetForm();
      })
      .catch(e => {
        const message = e && e.response && e.response.data && e.response.data.message;
        notification.fail(t(`errors:${message}`), { t });
      })
      .finally(() => setLoading(false));
  };

  return (
    <div className='login'>
      <div className='login-page'>
        <div className='login-page-logo-wrapper'>
          <img className='login-page-logo' src={ logo } alt=''/>
          <div className='login-page-label'>Solve.it</div>
        </div>
        <Tabs activeKey={ activeTab } onChange={ onTabClick } centered>
          <Tabs.TabPane tab={ t('common:login.tabs.login') } key={ ROUTES.LOGIN } className='login-page-tab'>
            <LoginForm onSubmit={ onLogin } loading={ loading }/>
          </Tabs.TabPane>
          <Tabs.TabPane tab={ t('common:login.tabs.register') } key={ ROUTES.REGISTER } className='login-page-tab'>
            <RegisterForm onSubmit={ onRegister } loading={ loading }/>
          </Tabs.TabPane>
        </Tabs>
      </div>
    </div>
  );
}

export default LoginPage;
