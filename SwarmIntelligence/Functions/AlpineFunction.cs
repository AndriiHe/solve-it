using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class AlpineFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            return values.Sum(x => Math.Abs(x * Math.Sin(x) + 0.1 * x));
        }
    }
}
