using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class GrivonkFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var sum = values.Sum(x => Math.Pow(x, 2));
            var prod = values.Select((x, i) => Math.Cos(x / Math.Sqrt(i + 1))).Aggregate(1.0, (acc, x) => acc * x);

            return 1.0 / 4000 * sum - prod + 1;
        }
    }
}
