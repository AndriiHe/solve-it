using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class WeierstrassFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var d = values.Length;
            var sum1 = 0.0;

            for (var i = 0; i < d; i++)
            {
                var sum3 = 0.0;
                for (var k = 0; k < 20; k++)
                    sum3 += Math.Pow(0.5, k) *Math.Cos(2.0 * Math.PI * Math.Pow(3, k) * (values[i] + 0.5));
                sum1 += sum3;
            }

            var sum2 = 0.0;
            for (var k = 0; k < 20; k++)
                sum2 += Math.Pow(0.5, k) * Math.Cos(2.0 * Math.PI * Math.Pow(3, k) * 0.5);

            return 1.0 / d * sum1 - sum2;
        }
    }
}
