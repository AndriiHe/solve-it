using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;
namespace SolveIt.SwarmIntelligence.Functions
{
    public class MichalewiczFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var d = values.Length;
            var sum = 0.0;
            for (var i = 0; i < d; i++)
                sum += Math.Sin(values[i]) * Math.Pow(Math.Sin((i + 1) * Math.Pow(values[i], 2) / Math.PI), 20);

            return -sum;
        }
    }
}
