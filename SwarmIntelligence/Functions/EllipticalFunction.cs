using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class EllipticalFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var d = values.Length;
            return values.Select((x, i) => Math.Pow(1000000, i / (d - 1.0)) * Math.Pow(x, 2)).Sum();
        }
    }
}
