using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class RastriginFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            return values.Sum(x => Math.Pow(x, 2) - 10 * Math.Cos(2 * Math.PI * x) + 10);
        }
    }
}
