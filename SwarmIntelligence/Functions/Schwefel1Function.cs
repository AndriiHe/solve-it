using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class Schwefel1Function : IFunction
    {
        public double GetValue(double[] values)
        {
            var sum = 0.0;
            for (var i = 0; i < values.Length; i++)
            {
                var nestedSum = 0.0;
                for (var j = 0; j <= i; j++)
                    nestedSum += values[j];
                sum += Math.Pow(nestedSum, 2);
            }

            return sum;
        }
    }
}
