using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SolveIt.MathParser;
using SolveIt.MathParser.Model;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class CustomFunction : IFunction
    {
        private readonly Expression _expression;
        private readonly ExpressionContext _context;

        public CustomFunction(string expression, IEnumerable<string> restrictions)
        {
            _expression = new Expression(expression);
            _context = new ExpressionContext();
            var functions = restrictions.ToDictionary(
                restriction => Regex.Match(restriction.Replace(" ", ""), @"\w*(?=\()").Value,
                restriction => new Expression(Regex.Match(restriction.Replace(" ", ""), @"(?<=(=>))(.*)$").Value)
            );

            foreach (var (name, function) in functions)
                _context.AddFunction(new Function(name, _ => function.Execute(_context)));
        }

        public CustomFunction(string expression)
        {
            _expression = new Expression(expression);
            _context = new ExpressionContext();
        }

        public double GetValue(double[] values)
        {
            _context.AddMatrix("x", values);
            _context.AddVariable("d", values.Length);

            return _expression.Execute(_context);
        }
    }
}
