using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class AbsoluteMaximumFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var max = Math.Abs(values[0]);
            for (var i = 0; i < values.Length; i++)
            {
                var value = Math.Abs(values[i]);
                if (value >= max)
                    max = value;
            }
            return max;
        }
    }
}
