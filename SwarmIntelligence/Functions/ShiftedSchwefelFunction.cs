using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class ShiftedSchwefelFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var sum = 0.0;
            var prod = 1.0;
            for (var i = 0; i < values.Length; i++)
                sum += Math.Abs(values[i]);
            for (var i = 0; i < values.Length; i++)
                prod *= Math.Abs(values[i]);
            return sum + prod;
        }
    }
}
