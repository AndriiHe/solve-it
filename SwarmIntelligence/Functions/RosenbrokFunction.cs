using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class RosenbrokFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var sum = 0.0;
            for (var i = 0; i < values.Length - 1; i++)
                sum += 100 * Math.Pow(values[i + 1] - Math.Pow(values[i], 2), 2) + Math.Pow(1 - values[i], 2);

            return sum;
        }
    }
}
