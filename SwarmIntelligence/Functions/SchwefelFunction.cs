using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class SchwefelFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var sum = 0.0;
            for (var i = 0; i < values.Length; i++)
                sum += values[i] * Math.Sin(Math.Sqrt(Math.Abs(values[i])));
            return -(1.0 / values.Length) * sum + 418.983;
        }
    }
}
