using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class AckleyFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var d = values.Length;
            return -20 * Math.Exp(-0.2 * Math.Sqrt(1.0 / d * values.Sum(x => Math.Pow(x, 2)))) - Math.Exp(1.0 / d * values.Sum(x => Math.Cos(2 * Math.PI * x))) + 20 + Math.E;
        }
    }
}
