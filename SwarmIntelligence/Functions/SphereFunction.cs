using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class SphereFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            return values.Sum(x => Math.Pow(x, 2));
        }
    }
}
