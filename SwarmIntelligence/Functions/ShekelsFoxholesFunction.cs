using System;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class ShekelsFoxholesFunction : IFunction
    {
        private static readonly double[,] A = {
            {-32, -16, 0.0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32},
            {-32, -32, -32, -32, -32, -16, -16, -16, -16, -16, 0, 0, 0, 0, 0, 16, 16, 16, 16, 16, 32, 32, 32, 32, 32}
        };
        public double GetValue(double[] values)
        {
            var sum = 0.0;
            for (var j = 0; j < 25; j++)
            {
                sum += 1 / (j + 1 + Math.Pow(values[0] - A[0, j], 6) + Math.Pow(values[1] - A[1, j], 6));
            }
            return Math.Pow(1.0 / 500 + sum, -1);
        }
    }
}
