using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class ZakharovFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            var sum = values.Select((x, i) => 0.5 * (i + 1) * x).Sum();
            return values.Sum(x => Math.Pow(x, 2)) + Math.Pow(sum, 2) + Math.Pow(sum, 4);
        }
    }
}
