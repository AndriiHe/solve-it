using System.Collections.Generic;
using System.Text.RegularExpressions;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public static class FunctionFactory
    {
        public static IFunction GetFunction(string expression, IEnumerable<string> restrictions = null)
        {
            var function = Regex.Replace(expression, @"\s+", "");
            return function switch
            {
                "sum(i,1,d,x[i]^2)" => new SphereFunction(),
                "sum(i,1,d-1,100*(x[i+1]-x[i]^2)^2+(1-x[i])^2)" => new RosenbrokFunction(),
                "sum(i,1,d,abs(x[i]*sin(x[i])+0.1*x[i]))" => new AlpineFunction(),
                "-(1/d)*sum(i,1,d,x[i]*sin(sqrt(abs(x[i]))))+418.983" => new SchwefelFunction(),
                "sum(i,1,d,(10^6)^((i-1)/(d-1))*x[i]^2)" => new EllipticalFunction(),
                "sum(i,1,d,x[i]^2-10*cos(2*pi*x[i])+10)" => new RastriginFunction(),
                "1/4000*sum(i,1,d,x[i]^2)-prod(i,1,d,cos(x[i]/sqrt(i)))+1" => new GrivonkFunction(),
                "sum(i,1,d,sum(j,1,i,x[j])^2)" => new Schwefel1Function(),
                "-20*exp(-0.2*sqrt(1/d*sum(i,1,d,x[i]^2)))-exp(1/d*sum(i,1,d,cos(2*pi*x[i])))+20+e" => new AckleyFunction(),
                "(1/d)*sum(i,1,d,sum(k,0,20,(0.5^k)*cos(2*pi*(3^k)*(x[i]+0.5))))-sum(k,0,20,(0.5^k)*cos(pi*3^k))" => new WeierstrassFunction(),
                "-sum(i,1,d,sin(x[i])*sin(i*(x[i]^2)/pi)^20)" => new MichalewiczFunction(),
                "1/2*sum(i,1,d,x[i]^4-16*x[i]^2+5*x[i])" => new StyblinskiTangFunction(),
                "sum(i,1,d,x[i]^2)+sum(i,1,d,0.5*i*x[i])^2+sum(i,1,d,0.5*i*x[i])^4" => new ZakharovFunction(),
                "sum(i,1,d,abs(x[i]))+prod(i,1,d,abs(x[i]))" => new ShiftedSchwefelFunction(),
                "max(i,1,d,abs(x[i]))" => new AbsoluteMaximumFunction(),
                "(1/500+sum(j,1,25,1/(j+sum(i,1,2,(x[i]-a[i,j])^6))))^(-1)" => new ShekelsFoxholesFunction(),
                _ => restrictions == null ? new CustomFunction(function) : new CustomFunction(function, restrictions)
            };
        }
    }
}
