namespace SolveIt.SwarmIntelligence.Functions.Interfaces
{
    public interface IFunction
    {
        public double GetValue(double[] values);
    }
}
