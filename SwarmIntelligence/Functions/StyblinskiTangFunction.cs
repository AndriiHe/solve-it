using System;
using System.Linq;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence.Functions
{
    public class StyblinskiTangFunction : IFunction
    {
        public double GetValue(double[] values)
        {
            return 1.0 / 2.0 * values.Sum(x => Math.Pow(x, 4) - 16 * Math.Pow(x, 2) + 5 * x);
        }
    }
}
