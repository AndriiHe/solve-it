using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms.Options;

namespace SolveIt.SwarmIntelligence.Algorithms.Interfaces
{
    public interface IAlgorithm
    {
        public Solution Solve(Problem problem, AlgorithmOptions options, LogProcess logType = 0);
    }
}
