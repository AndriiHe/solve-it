using System;
using System.Collections.Generic;
using System.Linq;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence.Algorithms.Interfaces;
using Range = SolveIt.Common.Model.Range;

namespace SolveIt.SwarmIntelligence.Algorithms
{
    public abstract class Algorithm : IAlgorithm
    {
        protected readonly Random Random = new Random();
        public abstract Solution Solve(Problem problem, AlgorithmOptions options, LogProcess logType = 0);

        protected static bool IsStopCriteriaAchieved(AlgorithmOptions options, Solution solution)
        {
            return solution.IterationsCount >= options.MaxIterationNumber | (solution.Accuracy <= options.Accuracy && solution.Accuracy > 0 && options.Accuracy > 0);
        }

        protected static double NormalizePosition(double value, Range bound)
        {
            return value > bound.Max ? bound.Max : value < bound.Min ? bound.Min : value;
        }

        protected double GetRandomPosition(Range bound)
        {
            return bound.Min + (bound.Max - bound.Min) * Random.NextDouble();
        }

        protected static SearchAgent GetBest(IEnumerable<SearchAgent> population)
        {
            return population.OrderBy(searchAgent => searchAgent.Value).First();
        }

        protected static SearchAgent GetBest(double[][] population, double[] fitness)
        {
            var best = 0;
            for (var i = 0; i < fitness.Length; i++)
            {
                if (fitness[best] >= fitness[i]) best = i;
            }

            return new SearchAgent(population[best], fitness[best]);
        }
    }
}
