using System.Linq;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms.Options;

namespace SolveIt.SwarmIntelligence.Algorithms
{
    public class StandardParticleSwarmAlgorithm : Algorithm
    {
        public override Solution Solve(Problem problem, AlgorithmOptions options, LogProcess logType = 0)
        {
            var opts = (StandardParticleSwarmOptions)options;
            var x = new double[options.AgentNumber].Select(_ => new double[problem.Bounds.Length]).ToArray();
            var p = new double[options.AgentNumber].Select(_ => new double[problem.Bounds.Length]).ToArray();
            var v = new double[options.AgentNumber].Select(_ => new double[problem.Bounds.Length]).ToArray();
            var fitness = new double[options.AgentNumber];
            var fp = new double[options.AgentNumber];
            var gbest = 0;

            for (var i = 0; i < options.AgentNumber; i++)
            {
                for (var j = 0; j < problem.Bounds.Length; j++)
                {
                    x[i][j] = problem.Bounds[j].Min + (problem.Bounds[j].Max - problem.Bounds[j].Min) * Random.NextDouble();
                    v[i][j] = problem.Bounds[j].Min + (problem.Bounds[j].Max - problem.Bounds[j].Min) / 2 * Random.NextDouble();
                    p[i][j] = x[i][j];
                }

                fitness[i] = problem.Function.GetValue(x[i]);
                fp[i] = fitness[i];
                gbest = fitness[i] < fitness[gbest] ? i : gbest;
            }

            var solution = new Solution(new SearchAgent(x[gbest], fitness[gbest]), x, logType);


            while (!IsStopCriteriaAchieved(options, solution))
            {
                var inertialWeight = opts.InertialWeight.Max - (opts.InertialWeight.Max - opts.InertialWeight.Min) / opts.MaxIterationNumber * solution.IterationsCount;
                for (var i = 0; i < options.AgentNumber; i++)
                {
                    for (var j = 0; j < problem.Bounds.Length; j++)
                    {
                        v[i][j] = v[i][j] * inertialWeight + opts.CognitiveCoefficient * Random.NextDouble() * (p[i][j] - x[i][j]);
                        if (i != gbest) v[i][j] += opts.SocialCoefficient * Random.NextDouble() * (p[gbest][j] - x[i][j]);
                        x[i][j] += v[i][j];

                        if (x[i][j] < problem.Bounds[j].Min)
                        {
                            x[i][j] = problem.Bounds[j].Min;
                            v[i][j] = 0;
                        }
                        else if (x[i][j] > problem.Bounds[j].Max)
                        {
                            x[i][j] = problem.Bounds[j].Max;
                            v[i][j] = 0;
                        }
                    }

                    fitness[i] = problem.Function.GetValue(x[i]);
                }

                for (var i = 0; i < options.AgentNumber; i++)
                {
                    if (fitness[i] < fp[i])
                    {
                        fp[i] = fitness[i];

                        for (var j = 0; j < problem.Bounds.Length; j++)
                        {
                            p[i][j] = x[i][j];
                        }
                    }
                }

                for (var i = 0; i < options.AgentNumber; i++)
                {
                    if (fp[i] < fp[gbest])
                    {
                        gbest = i;
                    }
                }

                solution.Update(new SearchAgent(p[gbest], fp[gbest]), x);
            }
            return solution;
        }
    }
}
