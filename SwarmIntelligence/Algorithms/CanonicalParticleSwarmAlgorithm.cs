using System.Collections.Generic;
using System.Linq;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence.Functions.Interfaces;
using SolveIt.SwarmIntelligence.SearchAgents;

namespace SolveIt.SwarmIntelligence.Algorithms
{
    public class CanonicalParticleSwarmAlgorithm : Algorithm
    {
        public override Solution Solve(Problem problem, AlgorithmOptions options, LogProcess logType = 0)
        {
            var population = GetInitialPopulation(options.AgentNumber, problem.Bounds, problem.Function);
            var solution = new Solution(GetBest(population), population, logType);
            while (!IsStopCriteriaAchieved(options, solution))
            {
                population = UpdatePosition(population, problem, (CanonicalParticleSwarmOptions)options);
                solution.Update(GetBest(population), population);
            }

            return solution;
        }

        private IList<Particle> GetInitialPopulation(int particles, Range[] bounds, IFunction function)
        {
            return new Particle[particles].Select(_ =>
            {
                var position = bounds.Select(GetRandomPosition).ToArray();
                var velocity = bounds.Select(GetRandomPosition).Select(value => value / 2).ToArray();
                return new Particle(position, function.GetValue(position))
                {
                    Velocity = velocity,
                    PersonalBest = new Particle(position, function.GetValue(position))
                };
            }).ToList();
        }

        private IList<Particle> UpdatePosition(IList<Particle> population, Problem problem, CanonicalParticleSwarmOptions options)
        {
            var best = GetBest(population);

            return population.Select(particle =>
            {
                var velocities = particle.Velocity.Select((_, i) => GetVelocity(particle, i, best, options)).ToArray();
                var position = particle.Position
                    .Select((coordinate, i) => coordinate + velocities[i])
                    .Select((coordinate, i) => NormalizePosition(coordinate, problem.Bounds[i]))
                    .ToArray();

                var value = problem.Function.GetValue(position);

                return new Particle(position, value)
                {
                    Velocity = velocities,
                    PersonalBest = particle.PersonalBest.Value <= value ? new Particle(position, value) : new Particle(particle.Position, particle.Value)
                };
            }).ToList();
        }

        private double GetVelocity(Particle particle, int dimension, SearchAgent best, CanonicalParticleSwarmOptions options)
        {
            var velocity = options.InertialCoefficient * particle.Velocity[dimension];
            var cognitive = options.CognitiveCoefficient * Random.NextDouble() * (particle.PersonalBest.Position[dimension] - particle.Position[dimension]);
            var social = options.SocialCoefficient * Random.NextDouble() * (best.Position[dimension] - particle.Position[dimension]);
            return velocity + cognitive + social;
        }
    }
}
