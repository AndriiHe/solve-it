using System;
using System.Linq;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms.Options;

namespace SolveIt.SwarmIntelligence.Algorithms
{
    public class BatGangAlgorithm : Algorithm
    {
        public override Solution Solve(Problem problem, AlgorithmOptions options, LogProcess logType = 0)
        {
            var opts = (BatGangOptions) options;
            var x = new double[options.AgentNumber].Select(_ => new double[problem.Bounds.Length]).ToArray();
            var v = new double[options.AgentNumber].Select(_ => new double[problem.Bounds.Length]).ToArray();
            var fitness = new double[options.AgentNumber];
            var bestPosition = new double[problem.Bounds.Length];
            var bestFitness = double.MaxValue;

            var a = new double[options.AgentNumber];
            var r = new double[options.AgentNumber];
            var r0 = new double[options.AgentNumber].Select(_ => opts.InitialPulseRate.Min + (opts.InitialPulseRate.Max - opts.InitialPulseRate.Min) * Random.NextDouble()).ToArray();


            for (var i = 0; i < options.AgentNumber; i++)
            {
                for (var j = 0; j < problem.Bounds.Length; j++)
                {
                    x[i][j] = problem.Bounds[j].Min + (problem.Bounds[j].Max - problem.Bounds[j].Min) * Random.NextDouble();
                }

                a[i] = opts.InitialLoudness.Min + (opts.InitialLoudness.Max - opts.InitialLoudness.Min) * Random.NextDouble();
                r[i] = r0[i];
                fitness[i] = problem.Function.GetValue(x[i]);
                bestPosition = fitness[i] < bestFitness ? x[i] : bestPosition;
                bestFitness = fitness[i] < bestFitness ? fitness[i] : bestFitness;
            }

            var solution = new Solution(new SearchAgent(bestPosition, bestFitness), x, logType);

            while (!IsStopCriteriaAchieved(options, solution))
            {
                var meanLoudness = a.Average();
                for (var i = 0; i < options.AgentNumber; i++)
                {
                    var position = new double[problem.Bounds.Length];
                    var f = opts.Frequency.Min + (opts.Frequency.Max - opts.Frequency.Min) * Random.NextDouble();
                    for (var j = 0; j < problem.Bounds.Length; j++)
                    {
                        v[i][j] += (x[i][j] - bestPosition[j]) * f;
                        position[j] = NormalizePosition(x[i][j] + v[i][j], problem.Bounds[j]);
                    }

                    if (Random.NextDouble() > r[i])
                        for (var j = 0; j < problem.Bounds.Length; j++)
                            position[j] = NormalizePosition(bestPosition[j] + (Random.NextDouble() > 0.5 ? -1.0 : 1.0) * Random.NextDouble() * meanLoudness, problem.Bounds[j]);


                    var value = problem.Function.GetValue(position);

                    if (value <= fitness[i] && Random.NextDouble() < a[i])
                    {
                        x[i] = position;
                        fitness[i] = value;
                        r[i] = r0[i] * (1 - Math.Exp(-1 * opts.Gamma * i));
                        a[i] = a[i] * opts.Alpha;
                    }

                    if (value <= bestFitness)
                    {
                        bestPosition = position;
                        bestFitness = value;
                    }
                }
                solution.Update(new SearchAgent(bestPosition, bestFitness), x);
            }

            return solution;
        }
    }
}
