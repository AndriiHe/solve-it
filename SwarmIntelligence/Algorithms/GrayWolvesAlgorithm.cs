using System;
using System.Collections.Generic;
using System.Linq;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms.Options;

namespace SolveIt.SwarmIntelligence.Algorithms
{
    public class GrayWolfsAlgorithm : Algorithm
    {
        public override Solution Solve(Problem problem, AlgorithmOptions options, LogProcess logType = 0)
        {
            var x = new double[options.AgentNumber].Select(_ => new double[problem.Bounds.Length]).ToArray();
            var fitness = new double[options.AgentNumber];
            var bestPosition = new double[problem.Bounds.Length];
            var bestFitness = double.MaxValue;

            for (var i = 0; i < options.AgentNumber; i++)
            {
                for (var j = 0; j < problem.Bounds.Length; j++)
                {
                    x[i][j] = problem.Bounds[j].Min + (problem.Bounds[j].Max - problem.Bounds[j].Min) * Random.NextDouble();
                }

                fitness[i] = problem.Function.GetValue(x[i]);
                bestPosition = fitness[i] < bestFitness ? x[i] : bestPosition;
                bestFitness = fitness[i] < bestFitness ? fitness[i] : bestFitness;
            }

            var solution = new Solution(new SearchAgent(bestPosition, bestFitness), x, logType);
            while (!IsStopCriteriaAchieved(options, solution))
            {
                var alpha = 2.0 - solution.IterationsCount * (2.0 / options.MaxIterationNumber);
                var leaders = GetLeaders(x, fitness);
                for (var i = 0; i < options.AgentNumber; i++)
                {
                    for (var j = 0; j < problem.Bounds.Length; j++)
                    {
                        x[i][j] = NormalizePosition(GetPosition(x[i][j], j, leaders, alpha), problem.Bounds[j]);
                    }

                    fitness[i] = problem.Function.GetValue(x[i]);
                }

                var best = GetBest(x, fitness);
                solution.Update(best, x);
            }

            return solution;
        }

        private static double[][] GetLeaders(IReadOnlyList<double[]> population, IEnumerable<double> fitness)
        {
            return fitness.Select((v, i) => new { v, i })
                .OrderBy(i => i.v)
                .Take(3)
                .Select(i => population[i.i])
                .ToArray();
        }

        private double GetPosition(double value, int dimension, IReadOnlyList<double[]> leaders, double alpha)
        {
            var sum = 0.0;
            for (var i = 0; i < leaders.Count; i++)
                sum = GetVector(value, leaders[i][dimension], alpha);

            return sum;
        }

        private double GetVector(double value, double leader, double alpha)
        {
            var a = 2.0 * alpha * Random.NextDouble() - alpha;
            var c = 2.0 * Random.NextDouble();
            return leader - a * Math.Abs(c * leader - value);
        }
    }
}
