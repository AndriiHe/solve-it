using System;
using System.Collections.Generic;
using System.Linq;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence.Functions.Interfaces;
using SolveIt.SwarmIntelligence.SearchAgents;
using Range = SolveIt.Common.Model.Range;

namespace SolveIt.SwarmIntelligence.Algorithms
{
    public class GravitationalSearchAlgorithm : Algorithm
    {
        public override Solution Solve(Problem problem, AlgorithmOptions options, LogProcess logType = 0)
        {
            var population = GetInitialPopulation(options.AgentNumber, problem.Bounds, problem.Function);
            var solution = new Solution(GetBest(population), population, logType);
            while (!IsStopCriteriaAchieved(options, solution))
            {
                var gravitationalConstant = GetGravitationalConstant(solution.IterationsCount, options.MaxIterationNumber);
                population = UpdatePosition(population, problem, gravitationalConstant);
                var best = GetBest(population);
                solution.Update(best.Value < solution.Value ? best : new Planet(solution.Position, solution.Value), population);
            }

            return solution;
        }

        private IList<Planet> GetInitialPopulation(int planets, ICollection<Range> bounds, IFunction function)
        {
            return new Planet[planets].Select(_ =>
            {
                var position = bounds.Select(GetRandomPosition).ToArray();
                return new Planet(position, function.GetValue(position))
                {
                    Velocity = new double[bounds.Count]
                };
            }).ToList();
        }

        private IList<Planet> UpdatePosition(IList<Planet> planets, Problem problem, double gravitationalConstant)
        {
            var best = GetBest(planets);
            var worst = GetWorst(planets);
            var masses = GetMasses(planets, best, worst).ToList();
            var population = new List<Planet>();

            for (var i = 0; i < planets.Count; i++)
            {
                var acceleration = GetAcceleration(planets, planets[i], masses).Select(a => a * gravitationalConstant).ToArray();
                var velocity = new double[planets[i].Velocity.Length];
                var position = new double[planets[i].Position.Length];
                for (var d = 0; d < planets[i].Position.Length; d++)
                {
                    velocity[d] = Random.NextDouble() * planets[i].Velocity[d] + acceleration[d];
                    position[d] = NormalizePosition(planets[i].Position[d] + velocity[d], problem.Bounds[d]);
                }

                population.Add(new Planet(position, problem.Function.GetValue(position))
                {
                    Mass = masses[i],
                    Velocity = velocity
                });
            }

            return population;
        }

        private static IEnumerable<double> GetMasses(IEnumerable<Planet> planets, SearchAgent best, SearchAgent worst)
        {
            var masses = planets.Select(planet => (planet.Value - worst.Value) / (best.Value - worst.Value)).ToArray();
            var totalMasses = masses.Sum();
            return masses.Select(mass => mass / totalMasses);
        }

        private IEnumerable<double> GetAcceleration(IList<Planet> planets, SearchAgent planet, IList<double> masses)
        {
            var acceleration = new double[planet.Position.Length];
            for (var j = 0; j < planets.Count; j++)
            {
                var distance = GetEuclidianDistance(planet.Position, planets[j].Position);
                for (var d = 0; d < planet.Position.Length; d++)
                    acceleration[d] += Random.NextDouble() * masses[j] * ((planets[j].Position[d] - planet.Position[d]) / (distance + 0.001));
            }

            return acceleration;
        }

        private static double GetEuclidianDistance(IList<double> x, IList<double> y)
        {
            var sum = 0.0;
            for (var i = 0; i < x.Count; i++)
                sum += Math.Pow(x[i] - y[i], 2);
            return Math.Sqrt(sum);
        }

        private static SearchAgent GetWorst(IEnumerable<Planet> population)
        {
            return population.OrderByDescending(searchAgent => searchAgent.Value).First().Clone();
        }

        private double GetGravitationalConstant(int iteration, int maxIterations)
        {
            return 100.0 * Math.Exp(-20.0 * iteration / maxIterations);
        }
    }
}
