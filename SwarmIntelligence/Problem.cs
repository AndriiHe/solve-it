using SolveIt.Common.Model;
using SolveIt.SwarmIntelligence.Functions.Interfaces;

namespace SolveIt.SwarmIntelligence
{
    public class Problem
    {
        public IFunction Function;
        public Range[] Bounds;
    }
}
