using SolveIt.Common.Model;

namespace SolveIt.SwarmIntelligence.SearchAgents
{
    public class Planet : SearchAgent
    {
        public double Mass;
        public double[] Velocity;
        public Planet(double[] position, double value) : base(position, value)
        {
        }

        public SearchAgent Clone()
        {
            return new Planet(Position, Value)
            {
                Mass = Mass,
                Velocity = Velocity,
            };
        }
    }
}
