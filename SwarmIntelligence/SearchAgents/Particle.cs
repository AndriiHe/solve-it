using SolveIt.Common.Model;

namespace SolveIt.SwarmIntelligence.SearchAgents
{
    public class Particle : SearchAgent
    {
        public double[] Velocity;
        public SearchAgent PersonalBest;

        public Particle(double[] position, double value) : base(position, value)
        {
        }
    }
}
