using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using SolveIt.Common.Model;
using SolveIt.Common.Model.Algorithms;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence;
using SolveIt.SwarmIntelligence.Algorithms;
using SolveIt.SwarmIntelligence.Functions;
using Range = SolveIt.Common.Model.Range;

namespace SolveIt.Tool
{
    class Program
    {
        static void Main()
        {
            Execute();
        }

        public static void Execute()
        {

        var algorithms = new Dictionary<SwarmAlgorithm, string>
            {
                { SwarmAlgorithm.CanonicalParticleSwarmAlgorithm, "PSO" },
                { SwarmAlgorithm.GrayWolfsAlgorithm, "Алгоритм зграї сірих вовків" },
                { SwarmAlgorithm.BatGangAlgorithm, "Кажанячий алгоритм" },
                { SwarmAlgorithm.GravitationalSearchAlgorithm, "Гравітаційний алгоритм" },
                { SwarmAlgorithm.StandardParticleSwarmAlgorithm, "SPSO" },
            };

            var functions = new List<FunctionInfo>
            {
                new FunctionInfo("Сферична функція", "sum(i,1,d,x[i]^2)", -20, 20),
                new FunctionInfo("Еліптична функція", "sum(i,1,d,(10^6)^((i-1)/(d-1))*x[i]^2)", -20, 20),
                new FunctionInfo("Гіпереліпсоїдна функція", "sum(i,1,d,sum(j,1,i,x[j]^2))", -65.536, 65.536),
                new FunctionInfo("Функція Розенброка", "sum(i,1,d-1,100*(x[i+1]-x[i]^2)^2+(1-x[i])^2)", -10, 10),
                new FunctionInfo("Функція Растригіна", "sum(i,1,d,x[i]^2-10*cos(2*pi*x[i])+10)", -5, 5),
                new FunctionInfo("Функція Грівонка", "1/4000*sum(i,1,d,x[i]^2)-prod(i,1,d,cos(x[i]/sqrt(i)))+1", -100, 100),
                new FunctionInfo("Функція Алпайна", "sum(i,1,d,abs(x[i]*sin(x[i])+0.1*x[i]))", -10, 10),
                new FunctionInfo("Функція Швефела", "-(1/d)*sum(i,1,d,x[i]*sin(sqrt(abs(x[i]))))+418.983", -500, 500),
                new FunctionInfo("Функція Швефела 1.2", "sum(i,1,d,sum(j,1,i,x[j])^2)", -10, 10),
                new FunctionInfo("Функція Аклея", "-20*exp(-0.2*sqrt(1/d*sum(i,1,d,x[i]^2)))-exp(1/d*sum(i,1,d,cos(2*pi*x[i])))+20+e", -30, 30),
                new FunctionInfo("Функція Вейеоштрасса", "(1/d)*sum(i,1,d,sum(k,0,20,(0.5^k)*cos(2*pi*(3^k)*(x[i]+0.5))))-sum(k,0,20,(0.5^k)*cos(pi*3^k))", 0, 0),
                new FunctionInfo("Функція Міхалевича", "-sum(i,1,d,sin(x[i])*sin(i*(x[i]^2)/pi)^20)", 0, 3.14),
                new FunctionInfo("Функція Стиблінского-танга", "1/2*sum(i,1,d,x[i]^4-16*x[i]^2+5*x[i])", -5, 5),
                new FunctionInfo("Функція Захарова", "sum(i,1,d,x[i]^2)+sum(i,1,d,0.5*i*x[i])^2+sum(i,1,d,0.5*i*x[i])^4", -5, 10),
                new FunctionInfo("Perm function", "sum(i,1,d,(sum(j,1,d,((j^i)+0.5)*(((x[j]/j)^i)-1)))^2)", -5, 5),
                new FunctionInfo("Perm function 0", "sum(i,1,d,(sum(j,1,d,(j+0.5)*((x[j]^i)-1/(j^i))))^2)", -5, 5),
                new FunctionInfo("Sum of different powers", "sum(i,1,d,abs(x[i])^(i+1))", -1, 1),
                new FunctionInfo("Trid function", "sum(i,1,d,(x[i]-1)^2)-sum(i,2,d,x[i]*x[i-1])", -10, 10),
                new FunctionInfo("Dixon-price function", "(x[1]-1)^2+sum(i,2,d,(2*(x[i]^2)-x[i-1])^2)", -10, 10),
                new FunctionInfo("Shifted’s Schwefel’s Problem", "sum(i,1,d,abs(x[i]))+prod(i,1,d,abs(x[i]))", -10, 10),
                new FunctionInfo("Absolute maximum function", "max(i,1,d,abs(x[i]))", -100, 100),
                new FunctionInfo("Shekel's Foxholes function", "(1/500+sum(j,1,25,1/(j+sum(i,1,2,(x[i]-a[i,j])^6))))^(-1)", -65.536, 65.536),
            };

            var algorithmParameters = new Dictionary<SwarmAlgorithm, List<AlgorithmParameter>>
            {
                {
                    SwarmAlgorithm.CanonicalParticleSwarmAlgorithm, new List<AlgorithmParameter>
                    {
                        new AlgorithmParameter("inertialCoefficient", "Введіть інерційний коефіцієнт (за замовчуванням 0,712):", "0.712"),
                        new AlgorithmParameter("cognitiveCoefficient", "Введіть когнітивний коефіцієнт (за замовчуванням 1,193):", "1.193"),
                        new AlgorithmParameter("socialCoefficient", "Введіть соціальний коефіцієнт (за замовчуванням 1,193):", "1.193"),
                    }
                },
                {
                    SwarmAlgorithm.StandardParticleSwarmAlgorithm, new List<AlgorithmParameter>
                    {
                        new AlgorithmParameter("minInertialCoefficient", "Введіть інерційний коефіцієнт min (за замовчуванням 0,4):", "0.4"),
                        new AlgorithmParameter("maxInertialCoefficient", "Введіть інерційний коефіцієнт max (за замовчуванням 0,9):", "0.9"),
                        new AlgorithmParameter("cognitiveCoefficient", "Введіть когнітивний коефіцієнт (за замовчуванням 1,193):", "1.193"),
                        new AlgorithmParameter("socialCoefficient", "Введіть соціальний коефіцієнт (за замовчуванням 1,193):", "1.193"),
                    }
                },
                {
                    SwarmAlgorithm.GrayWolfsAlgorithm, new List<AlgorithmParameter>()
                },
                {
                    SwarmAlgorithm.BatGangAlgorithm, new List<AlgorithmParameter>
                    {
                        new AlgorithmParameter("minFrequency", "Введіть мінімальну частоту (за замовчуванням 0):", "0"),
                        new AlgorithmParameter("maxFrequency", "Введіть максимальну частоту (за замовчуванням 2):", "2"),
                        new AlgorithmParameter("minInitialPulseRate", "Введіть мінімальний початковий коефіцієнт пульсації (за замовчуванням 0,25):", "0.25"),
                        new AlgorithmParameter("maxInitialPulseRate", "Введіть максимальний початковий коефіцієнт пульсації (за замовчуванням 0,35):", "0.35"),
                        new AlgorithmParameter("minInitialLoudness", "Введіть мінімальний початковий коефіцієнт гучності (за замовчуванням 0):", "0"),
                        new AlgorithmParameter("maxInitialLoudness", "Введіть максимальний початковий коефіцієнт гучності (за замовчуванням 0,35):", "0.35"),
                        new AlgorithmParameter("gamma", "Введіть гамма коефіцієнт (за замовчуванням 0,9):", "0.9"),
                        new AlgorithmParameter("alpha", "Введіть альфа коефіцієнт (за замовчуванням 0,9):", "0.9"),
                    }
                },
                {
                    SwarmAlgorithm.GravitationalSearchAlgorithm, new List<AlgorithmParameter>()
                }
            };

            Console.OutputEncoding = Encoding.UTF8;

            var userInput = new Dictionary<string, string>();
            Console.WriteLine("Оберіть алгоритм (за замовчуванням PSO):");
            algorithms.Keys.ToList().ForEach(a => Console.WriteLine($"{(int)a}) {algorithms.GetValueOrDefault(a)}"));

            var usedAlgorithms = ReadTrimmedLine("1");

            Console.WriteLine($"Введіть цільову функцію (за замовчуванням {functions[0].Name}):");
            Console.WriteLine("Наприклад:");

            functions.ForEach(f => Console.WriteLine($"{f.Name}: {f.Expression}"));
            var expression = ReadTrimmedLine(functions[0].Expression);

            Console.WriteLine("Введіть кількість функцій обмежень(за замовчуванням 0):");
            var restrictionsNumber = int.Parse(ReadTrimmedLine("0"));
            var restrictions = new List<string>();

            for (var i = 0; i < restrictionsNumber; i++)
            {
                Console.Write($"Введіть значення функції обмеження g{i+1}() => ");
                var restriction = $"g{i+1}() => " + ReadTrimmedLine("0");
                restrictions.Add(restriction);
            }

            var minBound = functions.FirstOrDefault(f => f.Expression == expression)?.MinBound ?? -10;
            var maxBound = functions.FirstOrDefault(f => f.Expression == expression)?.MaxBound ?? 10;

            Console.WriteLine("Введіть кількість повторів (за замовчуванням 1):");
            var times = int.Parse(ReadTrimmedLine("1"));

            Console.WriteLine("Введіть точність (за замовчуванням 0,00000001):");
            var accuracy = double.Parse(ReadTrimmedLine("0.00000001"));

            Console.WriteLine("Введіть максимальну кількість ітерацій (за замовчуванням 10 000):");
            var maxIterations = int.Parse(ReadTrimmedLine("10000"));

            Console.WriteLine("Введіть кількість пошукових агентів (за замовчуванням 50):");
            var agentNumber = int.Parse(ReadTrimmedLine("50"));

            Console.WriteLine("Введіть розмірність простору пошуку (за замовчуванням 2):");
            var dimensionsCount = int.Parse(ReadTrimmedLine("2"));

            Console.WriteLine("Простір пошуку є гіперкубом? Так - 1, Ні - 0 (за замовчуванням 1):");
            var isHyperCube = int.Parse(ReadTrimmedLine("1"));

            var bounds = new List<Range>();
            Console.WriteLine("Введіть межі простору пошуку:");

            if (isHyperCube == 1)
            {
                Console.WriteLine($"[d][min] (за замовчуванням {minBound}):");
                var min = double.Parse(ReadTrimmedLine($"{minBound}"));
                Console.WriteLine($"[d][max] (за замовчуванням {maxBound}):");
                var max = double.Parse(ReadTrimmedLine($"{maxBound}"));
                for (var i = 0; i < dimensionsCount; i++)
                {
                    bounds.Add(new Range(min, max));
                }
            }
            else
            {

                for (var i = 0; i < dimensionsCount; i++)
                {
                    Console.WriteLine($"[{i + 1}][min] (за замовчуванням {minBound}):");
                    var min = double.Parse(ReadTrimmedLine($"{minBound}"));
                    Console.WriteLine($"[{i + 1}][max] (за замовчуванням {maxBound}):");
                    var max = double.Parse(ReadTrimmedLine($"{maxBound}"));
                    bounds.Add(new Range(min, max));
                }
            }

            usedAlgorithms.Split(',').ToList().ForEach(a =>
            {
                var algorithm = (SwarmAlgorithm) int.Parse(a);
                Console.WriteLine($"{algorithms.GetValueOrDefault(algorithm)}\n");
                algorithmParameters.GetValueOrDefault(algorithm)?.ForEach(parameter =>
                {
                    Console.WriteLine(parameter.Question);
                    userInput.Add($"{(int)algorithm}-{parameter.Name}", ReadTrimmedLine(parameter.DefaultValue));
                });
            });

            var function = FunctionFactory.GetFunction(expression, restrictions);

            var problem = new Problem
            {
                Bounds = bounds.ToArray(),
                Function = function,
            };

            var bestSolutions = new List<Tuple<SwarmAlgorithm, Solution>>();

            usedAlgorithms.Split(',').ToList().ForEach(a =>
            {
                Console.WriteLine($"{algorithms.GetValueOrDefault((SwarmAlgorithm) int.Parse(a))}\n");
                var type = (SwarmAlgorithm) int.Parse(a);
                Algorithm algorithm = type switch
                {
                    SwarmAlgorithm.CanonicalParticleSwarmAlgorithm => new CanonicalParticleSwarmAlgorithm(),
                    SwarmAlgorithm.StandardParticleSwarmAlgorithm => new StandardParticleSwarmAlgorithm(),
                    SwarmAlgorithm.GrayWolfsAlgorithm => new GrayWolfsAlgorithm(),
                    SwarmAlgorithm.BatGangAlgorithm => new BatGangAlgorithm(),
                    SwarmAlgorithm.GravitationalSearchAlgorithm => new GravitationalSearchAlgorithm(),
                    _ => throw new ArgumentOutOfRangeException()
                };
                AlgorithmOptions options = type switch
                {
                    SwarmAlgorithm.CanonicalParticleSwarmAlgorithm => new CanonicalParticleSwarmOptions(agentNumber, maxIterations, accuracy)
                    {
                        InertialCoefficient = double.Parse(userInput.GetValueOrDefault("1-inertialCoefficient")!),
                        CognitiveCoefficient = double.Parse(userInput.GetValueOrDefault("1-cognitiveCoefficient")!),
                        SocialCoefficient = double.Parse(userInput.GetValueOrDefault("1-socialCoefficient")!),
                    },
                    SwarmAlgorithm.StandardParticleSwarmAlgorithm => new StandardParticleSwarmOptions(agentNumber, maxIterations, accuracy)
                    {
                        InertialWeight = new Range(double.Parse(userInput.GetValueOrDefault("5-minInertialCoefficient")!), double.Parse(userInput.GetValueOrDefault("5-maxInertialCoefficient")!)),
                        CognitiveCoefficient = double.Parse(userInput.GetValueOrDefault("5-cognitiveCoefficient")!),
                        SocialCoefficient = double.Parse(userInput.GetValueOrDefault("5-socialCoefficient")!),
                    },
                    SwarmAlgorithm.BatGangAlgorithm => new BatGangOptions(agentNumber, maxIterations, accuracy)
                    {
                        Alpha = double.Parse(userInput.GetValueOrDefault("3-alpha")!),
                        Gamma = double.Parse(userInput.GetValueOrDefault("3-gamma")!),
                        Frequency = new Range(double.Parse(userInput.GetValueOrDefault("3-minFrequency")!), double.Parse(userInput.GetValueOrDefault("3-maxFrequency")!)),
                        InitialPulseRate = new Range(double.Parse(userInput.GetValueOrDefault("3-minInitialPulseRate")!), double.Parse(userInput.GetValueOrDefault("3-maxInitialPulseRate")!)),
                        InitialLoudness = new Range(double.Parse(userInput.GetValueOrDefault("3-minInitialLoudness")!), double.Parse(userInput.GetValueOrDefault("3-maxInitialLoudness")!)),
                    },
                    SwarmAlgorithm.GravitationalSearchAlgorithm => new GravitationalSearchOptions(agentNumber, maxIterations, accuracy),
                    SwarmAlgorithm.GrayWolfsAlgorithm => new GrayWolvesOptions(agentNumber, maxIterations, accuracy),
                    _ => throw new ArgumentOutOfRangeException()
                };

                var solutions = new List<Solution>();

                for (var i = 0; i < times; i++)
                {
                    var start = DateTime.Now;
                    var solution = algorithm.Solve(problem, options);
                    var end = DateTime.Now;
                    solutions.Add(solution);
                    var position = string.Join(", ", solution?.Position ?? Array.Empty<double>());
                    Console.WriteLine($"f(x) = {solution?.Value}, x = [{position}]), k = {solution?.IterationsCount}");
                    Console.WriteLine($"Час виконання: {end - start}");
                }

                var bestSolution = solutions.OrderBy(i => i.Value).FirstOrDefault();
                bestSolutions.Add(new Tuple<SwarmAlgorithm, Solution>((SwarmAlgorithm) int.Parse(a), bestSolution));
            });

            bestSolutions.ForEach(a =>
            {
                var (i, s) = a;
                var position = string.Join(", ", s?.Position ?? Array.Empty<double>());
                Console.WriteLine($"\nНайкраще значення ({algorithms.GetValueOrDefault(i)})\n");
                Console.WriteLine($"f(x) = {s?.Value}, x = [{position}]), k = {s?.IterationsCount}");
            });

            Console.ReadLine();
        }

        private static string ReadTrimmedLine(string defaultValue)
        {
            var input = Console.ReadLine();
            return Regex.Replace(string.IsNullOrEmpty(input) ? defaultValue : input, @"\s+", "");
        }
    }

    class FunctionInfo
    {
        public string Name { get; set; }
        public string Expression { get; set; }
        public double MinBound { get; set; }
        public double MaxBound { get; set; }

        public FunctionInfo(string name, string expression, double minBound, double maxBound)
        {
            Name  = name;
            Expression = expression;
            MinBound = minBound;
            MaxBound = maxBound;
        }
    }

    class AlgorithmParameter
    {
        public string Name { get; set; }
        public string Question { get; set; }
        public string DefaultValue { get; set; }

        public AlgorithmParameter(string name, string question, string defaultValue)
        {
            Name = name;
            Question = question;
            DefaultValue = defaultValue;
        }
    }
}
