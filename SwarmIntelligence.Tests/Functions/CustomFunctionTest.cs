using NUnit.Framework;
using SolveIt.SwarmIntelligence.Functions;

namespace SolveIt.SwarmIntelligence.Tests.Functions
{
    public class CustomFunctionTest
    {
        [Test]
        public void Should_Calculate_Expression_Value()
        {
            var x = new double[] { 1, 2, 3 };
            Assert.AreEqual(new SphereFunction().GetValue(x), new CustomFunction("sum(i,1,d,x[i]^2)").GetValue(x));
            Assert.AreEqual(new RosenbrokFunction().GetValue(x), new CustomFunction("sum(i,1,d-1,100*(x[i+1]-x[i]^2)^2+(1-x[i])^2)").GetValue(x));
            Assert.AreEqual(new AlpineFunction().GetValue(x), new CustomFunction("sum(i,1,d,abs(x[i]*sin(x[i])+0.1*x[i]))").GetValue(x));
            Assert.AreEqual(new SchwefelFunction().GetValue(x), new CustomFunction("-(1/d)*sum(i,1,d,x[i]*sin(sqrt(abs(x[i]))))+418.983").GetValue(x));
            Assert.AreEqual(new EllipticalFunction().GetValue(x), new CustomFunction("sum(i,1,d,(10^6)^((i-1)/(d-1))*x[i]^2)").GetValue(x));
            Assert.AreEqual(new RastriginFunction().GetValue(x), new CustomFunction("sum(i,1,d,x[i]^2-10*cos(2*pi*x[i])+10)").GetValue(x));
            Assert.AreEqual(new GrivonkFunction().GetValue(x), new CustomFunction( "1/4000*sum(i,1,d,x[i]^2)-prod(i,1,d,cos(x[i]/sqrt(i)))+1").GetValue(x));
            Assert.AreEqual(new Schwefel1Function().GetValue(x), new CustomFunction("sum(i,1,d,sum(j,1,i,x[j])^2)").GetValue(x));
            Assert.AreEqual(new AckleyFunction().GetValue(x), new CustomFunction("-20*exp(-0.2*sqrt(1/d*sum(i,1,d,x[i]^2)))-exp(1/d*sum(i,1,d,cos(2*pi*x[i])))+20+e").GetValue(x));
            Assert.That(new CustomFunction("(1/d)*sum(i,1,d,sum(k,0,20,(0.5^k)*cos(2*pi*(3^k)*(x[i]+0.5))))-sum(k,0,20,(0.5^k)*cos(pi*3^k))").GetValue(x), Is.EqualTo(new WeierstrassFunction().GetValue(x)).Within(0.00001));
            Assert.AreEqual(new MichalewiczFunction().GetValue(x), new CustomFunction("-sum(i,1,d,sin(x[i])*sin(i*(x[i]^2)/pi)^20)").GetValue(x));
            Assert.AreEqual(new StyblinskiTangFunction().GetValue(x), new CustomFunction("1/2*sum(i,1,d,x[i]^4-16*x[i]^2+5*x[i])").GetValue(x));
            Assert.AreEqual(new ZakharovFunction().GetValue(x), new CustomFunction("sum(i,1,d,x[i]^2)+sum(i,1,d,0.5*i*x[i])^2+sum(i,1,d,0.5*i*x[i])^4").GetValue(x));
        }

        [Test]
        public void Should_Calculate_Expression_Value_With_Restrictions()
        {
            const string expression = "-100*x[1]-150*x[2]-300*x[3]-200*x[4]-100*x[5]+(1000*((max(0, g1()))^2+(max(0, g2()))^2+(max(0, g3()))^2+(max(0, g4()))^2+(max(0, g5()))^2))";
            var x = new[] {45.81, 210.73, 34.36, 31.87, 219.12};
            var restrictions = new[]
            {
                "g1()=>1.0570*x[1]+0.0628*x[2]+0.0891*x[3]+0.3301*x[4]+0.1164*x[5]-300",
                "g2()=>0.0411*x[1]+1.0270*x[2]+0.0209*x[3]+0.0240*x[4]+0.0667*x[5]-500",
                "g3()=>0.3450*x[1]+0.0461*x[2]+1.0833*x[3]+0.2554*x[4]+0.1226*x[5]-400",
                "g4()=>0.1211*x[1]+0.1187*x[2]+0.1720*x[3]+1.5021*x[4]+0.1175*x[5]-400",
                "g5()=>0.1029*x[1]+0.0822*x[2]+0.1404*x[3]+0.2207*x[4]+1.2127*x[5]-300",
            };
            Assert.AreEqual(-74784.5, new CustomFunction(expression, restrictions).GetValue(x));
        }
    }
}
