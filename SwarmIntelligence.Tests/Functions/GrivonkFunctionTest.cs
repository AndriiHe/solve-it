using NUnit.Framework;
using SolveIt.SwarmIntelligence.Functions;

namespace SolveIt.SwarmIntelligence.Tests.Functions
{
    public class GrivonkFunctionTest
    {
        [Test]
        public void Should_Calculate_Expression_Value()
        {
            Assert.AreEqual(new GrivonkFunction().GetValue(new double[] {0, 0}), 0);
        }
    }
}
