using NUnit.Framework;
using SolveIt.SwarmIntelligence.Algorithms;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence.Functions;
using Range = SolveIt.Common.Model.Range;

namespace SolveIt.SwarmIntelligence.Tests.Algorithms
{
    public class BatGangAlgorithmTest
    {
        [Test]
        public void Should_Found_Optimal_Solution_For_Custom_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new CustomFunction("sum(i, 1, d, x[i]^2)"),
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(40, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Sphere_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new SphereFunction(),
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(40, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Alpine_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new AlpineFunction(),
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(40, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Rosenbrok_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new RosenbrokFunction(),
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(40, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Schwefel_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-500, 500), new Range(-500, 500) },
                Function = new SchwefelFunction(),
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(500, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Elliptical_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new EllipticalFunction()
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(40, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Rastrigin_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-5, 5), new Range(-5, 5) },
                Function = new RastriginFunction(),
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(1000, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Grivonk_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-100, 100), new Range(-100, 100) },
                Function = new GrivonkFunction()
            };
            var algorithm = new BatGangAlgorithm();
            var options = new BatGangOptions(1000, 10000, 0.00000001)
            {
                Alpha = 0.9,
                Gamma = 0.9,
                Frequency = new Range(0, 2),
                InitialPulseRate = new Range(0.25, 0.35),
                InitialLoudness = new Range(0, 0.35)
            };
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.01));
        }
    }
}
