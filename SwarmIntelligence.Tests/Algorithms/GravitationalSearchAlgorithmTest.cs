using NUnit.Framework;
using SolveIt.Common.Model;
using SolveIt.SwarmIntelligence.Algorithms;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence.Functions;

namespace SolveIt.SwarmIntelligence.Tests.Algorithms
{
    public class GravitationalSearchAlgorithmTest
    {
        [Test]
        public void Should_Found_Optimal_Solution_For_Custom_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new CustomFunction("sum(i, 1, d, x[i]^2)"),
            };
            var options = new GravitationalSearchOptions(100, 200, 0.00000001);
            var algorithm = new GravitationalSearchAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Sphere_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-5, 5), new Range(-5, 5) },
                Function = new SphereFunction(),
            };
            var options = new GravitationalSearchOptions(100, 1000, 0.00000001);
            var algorithm = new GravitationalSearchAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Alpine_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new AlpineFunction(),
            };
            var options = new GravitationalSearchOptions(100, 1000, 0.00000001);
            var algorithm = new GravitationalSearchAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Rosenbrok_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-5, 5), new Range(-5, 5) },
                Function = new RosenbrokFunction(),
            };
            var options = new GravitationalSearchOptions(50, 5000, 0.00000001);
            var algorithm = new GravitationalSearchAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.005));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Schwefel_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(300, 500), new Range(300, 500) },
                Function = new SchwefelFunction(),
            };
            var options = new GravitationalSearchOptions(100, 1000, 0.00000001);
            var algorithm = new GravitationalSearchAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }
    }
}
