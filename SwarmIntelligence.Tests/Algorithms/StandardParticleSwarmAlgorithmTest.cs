using NUnit.Framework;
using SolveIt.SwarmIntelligence.Algorithms;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence.Functions;
using Range = SolveIt.Common.Model.Range;

namespace SolveIt.SwarmIntelligence.Tests.Algorithms
{
    public class StandardParticleSwarmAlgorithmTest
    {
        [Test]
        public void Should_Found_Optimal_Solution_For_Custom_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new CustomFunction("sum(i, 1, d, x[i]^2)"),
            };
            var options = new StandardParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Sphere_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new SphereFunction(),
            };
            var options = new StandardParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Alpine_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new AlpineFunction(),
            };
            var options = new StandardParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Rosenbrok_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new RosenbrokFunction(),
            };
            var options = new StandardParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Schwefel_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-500, 500), new Range(-500, 500) },
                Function = new SchwefelFunction(),
            };
            var options = new StandardParticleSwarmOptions(1000, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Elliptical_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new EllipticalFunction(),
            };
            var options = new StandardParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Rastrigin_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-5, 5), new Range(-5, 5) },
                Function = new RastriginFunction(),
            };
            var options = new StandardParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Grivonk_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-100, 100), new Range(-100, 100) },
                Function = new GrivonkFunction(),
            };
            var options = new StandardParticleSwarmOptions(1000, 10000, 0.00000001)
            {
                InertialWeight = new Range(0.4, 0.9),
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new StandardParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }
    }
}
