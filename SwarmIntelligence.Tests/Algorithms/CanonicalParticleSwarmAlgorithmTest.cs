using NUnit.Framework;
using SolveIt.Common.Model;
using SolveIt.SwarmIntelligence.Algorithms;
using SolveIt.Common.Model.Algorithms.Options;
using SolveIt.SwarmIntelligence.Functions;

namespace SolveIt.SwarmIntelligence.Tests.Algorithms
{
    public class CanonicalParticleSwarmAlgorithmTest
    {
        [Test]
        public void Should_Found_Optimal_Solution_For_Custom_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new CustomFunction("sum(i, 1, d-1, 100 * (x[i+1] - x[i]^2)^2 + (1 - x[i])^2)"),
            };
            var options = new CanonicalParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Sphere_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new SphereFunction(),
            };
            var options = new CanonicalParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Alpine_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new AlpineFunction(),
            };
            var options = new CanonicalParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Rosenbrok_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new RosenbrokFunction(),
            };
            var options = new CanonicalParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Schwefel_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-500, 500), new Range(-500, 500) },
                Function = new SchwefelFunction(),
            };
            var options = new CanonicalParticleSwarmOptions(500, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Elliptical_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-10, 10), new Range(-10, 10) },
                Function = new EllipticalFunction(),
            };
            var options = new CanonicalParticleSwarmOptions(100, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Rastrigin_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-5, 5), new Range(-5, 5) },
                Function = new RastriginFunction(),
            };
            var options = new CanonicalParticleSwarmOptions(500, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }

        [Test]
        public void Should_Found_Optimal_Solution_For_Grivonk_Function()
        {
            var problem = new Problem
            {
                Bounds = new[] { new Range(-100, 100), new Range(-100, 100) },
                Function = new GrivonkFunction(),
            };
            var options = new CanonicalParticleSwarmOptions(500, 10000, 0.00000001)
            {
                InertialCoefficient = 0.712,
                CognitiveCoefficient = 1.193,
                SocialCoefficient = 1.193
            };
            var algorithm = new CanonicalParticleSwarmAlgorithm();
            var solution = algorithm.Solve(problem, options);
            Assert.That(solution.Value, Is.EqualTo(0).Within(0.001));
        }
    }
}
